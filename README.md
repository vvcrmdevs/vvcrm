# Viking Vehicles CRM 

This git repository contains the MVP Version 1.0.0 of VikingVehicles CRM : VVCRM v1.0
and the Netbeans project file used in the development



### What's in this repository ###
##### This VVCRM repo.  A web app based on SuiteCRM 7.5

+ [VVCRM MVP version 1.0.0]: http://54.206.19.37
 
### SuiteCRM links:
+ [suitecrm_demo]: https://suitecrm.com/demo
+ [suitecrm_forums]: https://suitecrm.com/index.php?option=com_kunena&view=category&Itemid=1137&layout=list
+ [suitecrm_docs]: https://suitecrm.com/wiki
+ [suitecrm_partners]: https://suitecrm.com/index.php?option=com_content&view=article&id=170&Itemid=1172
+ [suitecrm_ext]: https://suitecrm.com/index.php?option=com_mtree&view=listcats&cat_id=76&Itemid=1225


### Support###
wf@whitehack.com.au