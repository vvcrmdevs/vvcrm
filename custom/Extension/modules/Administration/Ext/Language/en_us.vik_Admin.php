<?php

$mod_strings['LBL_VIK_ADMIN_LINK1_TITLE'] = 'Viking Vehicles Settings'; 
$mod_strings['LBL_VIK_ADMIN_LINK1_DESCRIPTION'] = 
        'LCT thresholds and fee structure for current financial year.';

$mod_strings['LBL_VIK_ADMIN_LINK2_TITLE'] = 'LCT thresholds for next financial year'; 
$mod_strings['LBL_VIK_ADMIN_LINK2_DESCRIPTION'] = 
        'LCT fuel-efficient thresholds for next financial year.';

?>