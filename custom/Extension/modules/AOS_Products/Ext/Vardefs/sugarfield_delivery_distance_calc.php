<?php
 // created: 2016-05-31 05:51:05
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['name']='delivery_distance_calc';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['vname']='LBL_DELIVERY_DISTANCE_CALC';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['type']='varchar';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['len']='16';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['source']='non-db';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['studio']='visible';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['inline_edit']='';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['importable']='false';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['merge_filter']='disabled';

 ?>