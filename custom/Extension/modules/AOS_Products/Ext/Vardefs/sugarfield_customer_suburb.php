<?php

 // created: 26/05/2016
// Author: Bill Forsyth
// non-db field used to to create customer suburb field which is used to calculate
//  customer distance
// modded 31/05/2016
$dictionary['AOS_Products']['fields']['customer_suburb']= array (
    'name' => 'customer_suburb',
    'vname' => 'LBL_CUST_SUBURBS_LKP',
    'type' => 'varchar',
    'len' => '48',
    'studio' => 'visible',    
    'source' => 'non-db',
);

