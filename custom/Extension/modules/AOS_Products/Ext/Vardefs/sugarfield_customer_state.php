<?php

 // created: 26/05/2016
// Author: Bill Forsyth
// non-db field used to to create customer state field which is used to calculate
//  customer distance
// modded 31/05/2016
$dictionary['AOS_Products']['fields']['customer_state']= array (
    'name' => 'customer_state',
    'vname' => 'LBL_CUST_STATE_LKP',
    'type' => 'varchar',
    'len' => '24',
    'studio' => 'visible',    
    'source' => 'non-db',
);

