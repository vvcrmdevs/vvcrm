<?php
 // created: 2016-04-20 02:31:34
$layout_defs["AOS_Contracts"]["subpanel_setup"]['vik_dealership_aos_contracts_1'] = array (
  'order' => 100,
  'module' => 'vik_Dealership',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_VIK_DEALERSHIP_AOS_CONTRACTS_1_FROM_VIK_DEALERSHIP_TITLE',
  'get_subpanel_data' => 'vik_dealership_aos_contracts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
