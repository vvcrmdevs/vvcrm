<?php
// created: 2016-04-20 02:28:50
$dictionary["vik_Dealership"]["fields"]["vik_dealership_aos_invoices_1"] = array (
  'name' => 'vik_dealership_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);
