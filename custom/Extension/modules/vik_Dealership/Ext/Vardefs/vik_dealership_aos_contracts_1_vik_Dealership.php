<?php
// created: 2016-04-20 02:32:39
$dictionary["vik_Dealership"]["fields"]["vik_dealership_aos_contracts_1"] = array (
  'name' => 'vik_dealership_aos_contracts_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_contracts_1',
  'source' => 'non-db',
  'module' => 'AOS_Contracts',
  'bean_name' => 'AOS_Contracts',
  'side' => 'right',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_CONTRACTS_1_FROM_AOS_CONTRACTS_TITLE',
);
