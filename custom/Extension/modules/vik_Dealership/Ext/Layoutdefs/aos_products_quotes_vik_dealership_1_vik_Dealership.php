<?php
 // created: 2016-04-27 04:09:22
$layout_defs["vik_Dealership"]["subpanel_setup"]['aos_products_quotes_vik_dealership_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products_Quotes',
  'subpanel_name' => 'ForAccounts',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_QUOTES_VIK_DEALERSHIP_1_FROM_AOS_PRODUCTS_QUOTES_TITLE',
  'get_subpanel_data' => 'aos_products_quotes_vik_dealership_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
