<?php
 // created: 2016-04-20 02:28:14
$layout_defs["vik_Dealership"]["subpanel_setup"]['vik_dealership_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'ForProductPurchases',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_VIK_DEALERSHIP_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
  'get_subpanel_data' => 'vik_dealership_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
