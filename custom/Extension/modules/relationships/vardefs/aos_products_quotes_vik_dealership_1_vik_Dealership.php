<?php
// created: 2016-04-27 04:09:22
$dictionary["vik_Dealership"]["fields"]["aos_products_quotes_vik_dealership_1"] = array (
  'name' => 'aos_products_quotes_vik_dealership_1',
  'type' => 'link',
  'relationship' => 'aos_products_quotes_vik_dealership_1',
  'source' => 'non-db',
  'module' => 'AOS_Products_Quotes',
  'bean_name' => 'AOS_Products_Quotes',
  'vname' => 'LBL_AOS_PRODUCTS_QUOTES_VIK_DEALERSHIP_1_FROM_AOS_PRODUCTS_QUOTES_TITLE',
);
