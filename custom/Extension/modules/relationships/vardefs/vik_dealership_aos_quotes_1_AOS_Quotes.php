<?php
// created: 2016-04-20 02:28:14
$dictionary["AOS_Quotes"]["fields"]["vik_dealership_aos_quotes_1"] = array (
  'name' => 'vik_dealership_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'vik_Dealership',
  'bean_name' => 'vik_Dealership',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_QUOTES_1_FROM_VIK_DEALERSHIP_TITLE',
);
