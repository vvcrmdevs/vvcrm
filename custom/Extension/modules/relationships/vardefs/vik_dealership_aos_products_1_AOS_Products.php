<?php
// created: 2016-04-20 02:26:48
$dictionary["AOS_Products"]["fields"]["vik_dealership_aos_products_1"] = array (
  'name' => 'vik_dealership_aos_products_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_products_1',
  'source' => 'non-db',
  'module' => 'vik_Dealership',
  'bean_name' => 'vik_Dealership',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_PRODUCTS_1_FROM_VIK_DEALERSHIP_TITLE',
  'id_name' => 'vik_dealership_aos_products_1vik_dealership_ida',
);
$dictionary["AOS_Products"]["fields"]["vik_dealership_aos_products_1_name"] = array (
  'name' => 'vik_dealership_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_PRODUCTS_1_FROM_VIK_DEALERSHIP_TITLE',
  'save' => true,
  'id_name' => 'vik_dealership_aos_products_1vik_dealership_ida',
  'link' => 'vik_dealership_aos_products_1',
  'table' => 'vik_dealership',
  'module' => 'vik_Dealership',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["vik_dealership_aos_products_1vik_dealership_ida"] = array (
  'name' => 'vik_dealership_aos_products_1vik_dealership_ida',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
