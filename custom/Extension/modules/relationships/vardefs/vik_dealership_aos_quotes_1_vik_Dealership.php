<?php
// created: 2016-04-20 02:28:14
$dictionary["vik_Dealership"]["fields"]["vik_dealership_aos_quotes_1"] = array (
  'name' => 'vik_dealership_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);
