<?php
// created: 2016-04-20 02:28:50
$dictionary["AOS_Invoices"]["fields"]["vik_dealership_aos_invoices_1"] = array (
  'name' => 'vik_dealership_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'vik_Dealership',
  'bean_name' => 'vik_Dealership',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_INVOICES_1_FROM_VIK_DEALERSHIP_TITLE',
);
