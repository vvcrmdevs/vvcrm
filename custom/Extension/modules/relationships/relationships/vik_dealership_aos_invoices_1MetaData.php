<?php
// created: 2016-04-20 02:28:50
$dictionary["vik_dealership_aos_invoices_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'vik_dealership_aos_invoices_1' => 
    array (
      'lhs_module' => 'vik_Dealership',
      'lhs_table' => 'vik_dealership',
      'lhs_key' => 'id',
      'rhs_module' => 'AOS_Invoices',
      'rhs_table' => 'aos_invoices',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'vik_dealership_aos_invoices_1_c',
      'join_key_lhs' => 'vik_dealership_aos_invoices_1vik_dealership_ida',
      'join_key_rhs' => 'vik_dealership_aos_invoices_1aos_invoices_idb',
    ),
  ),
  'table' => 'vik_dealership_aos_invoices_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'vik_dealership_aos_invoices_1vik_dealership_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'vik_dealership_aos_invoices_1aos_invoices_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'vik_dealership_aos_invoices_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'vik_dealership_aos_invoices_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'vik_dealership_aos_invoices_1vik_dealership_ida',
        1 => 'vik_dealership_aos_invoices_1aos_invoices_idb',
      ),
    ),
  ),
);