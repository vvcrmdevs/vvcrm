<?php
 // created: 2016-05-12 23:18:03
$dictionary['AOS_Products_Quotes']['fields']['discount']['default']='monthly';
$dictionary['AOS_Products_Quotes']['fields']['discount']['len']=100;
$dictionary['AOS_Products_Quotes']['fields']['discount']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['discount']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['discount']['help']='available vehicle discounts';
$dictionary['AOS_Products_Quotes']['fields']['discount']['importable']='false';

 ?>