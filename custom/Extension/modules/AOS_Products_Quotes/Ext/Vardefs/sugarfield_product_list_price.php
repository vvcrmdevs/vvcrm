<?php
 // created: 2016-05-14 10:13:54
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['enable_range_search']='1';
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['required']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['options']='numeric_range_search_dom';

 ?>