<?php
// @NOTE:
//      custom/Extension/application/Ext/Utils/xeromys_utils.php (this file)
//          becomes (after suiteCRM admin  repair/rebuild):
//      custom/application/Ext/Utils/custom_utils.ext.php
// 
// @Author: Bill Forsyth
// @Project: Viking Vehicles project
// @Date: 07/04/2016
// @description: populate independent dropdown lists for dynamic searches 
// 
function get_brands() {
    $list = array();
    $list[''] = 'All brands';
    $query = "SELECT DISTINCT  name as id,  name" .
            " FROM aos_products" .
            " where LENGTH(name) > 0 and deleted = 0" .
            " order by name asc; ";
    $result = $GLOBALS['db']->query($query, false);
    while (($row = $GLOBALS['db']->fetchByAssoc($result)) != null) {
        $list[$row['id']] = $row['name'];
    }
    return $list;
}
//
//
function get_states() {
    $list = array();
    $list[''] = 'All states';
    
    $query = "SELECT DISTINCT state as id, state"
            . " FROM vik_postcode"
           . "  where length(state)>0 "
            . " order by state asc;";
    $result = $GLOBALS['db']->query($query, false);
    while (($row = $GLOBALS['db']->fetchByAssoc($result)) != null) {
        $list[$row['id']] = $row['state'];
    }
    return $list;
}


?>

