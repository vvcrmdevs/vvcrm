<?php

/* 
 * @Author: Bill Forsyth
 * @date 10/05/2016
 * @description Entry point for Viking Vehicles Quotes from Vehicles list view
 */

$entry_point_registry['vik_Quotes_EntryPoint'] = 
                array(
                        'file' => 'custom/vik_EntryPoint.php',
                        'auth' => true,
                     );


