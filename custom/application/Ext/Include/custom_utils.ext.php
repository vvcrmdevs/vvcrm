<?php 
 //WARNING: The contents of this file are auto-generated


// @Author: Bill Forsyth
// @Project: Viking Vehicles project
// @Date: 07/04/2016
// @description: get dropdown lists for dynamic searches 
//
function get_brands()
{
    $query = "SELECT DISTINCT  names as id,  name".
             " FROM aos_products".
            " where LENGTH(name) > 0 ".
            " order by name asc; ";
        
    $result = $GLOBALS['db']->query($query, false);
    $list = array();
    $list['']='';
    while (($row = $GLOBALS['db']->fetchByAssoc($result)) != null) {
        $list[$row['id']] = $row['name'];      
    }
    return $list;
}

function get_models()
{
        $query = "SELECT DISTINCT concat(c.brand_c,'_',p.name) as id, p.name".
             " FROM aos_products_cstm c".
             " inner join aos_products p on p.id = c.id_c".
            " where length(c.brand_c) > 0".
           //  " where lower(c.brand_c) like '" . strtolower($brand) . "'".
            "  order by p.name asc;";
    
    $result = $GLOBALS['db']->query($query, false);
    $list = array();
    $list['']='';
    while (($row = $GLOBALS['db']->fetchByAssoc($result)) != null) {
           $list[$row['id']] = $row['name'];       
    }
    return $list;
}

function get_variants()        
{
    $query = "SELECT DISTINCT concat(c.brand_c,'_',p.name, '_', p.part_number) as id , p.part_number as name" .     
            " FROM aos_products_cstm c".
            " inner join aos_products p on p.id = c.id_c". 
             " where length(c.brand_c) > 0".
          //  " where lower(c.brand_c) like '" . strtolower($brand) . "'" .
          //  " and lower(p.name) like '" . strtolower($model) . "'" .
            " order by p.part_number asc;"; 
    
    $result = $GLOBALS['db']->query($query, false);
    $list = array();
    $list['']='';
    while (($row = $GLOBALS['db']->fetchByAssoc($result)) != null) {
        $list[$row['id']] = $row['name'];        
    }
    return $list;
}





?>