<?php
// created: 2016-06-09 04:12:44
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Calendar',
      2 => 'Accounts',
      3 => 'Contacts',
      4 => 'Opportunities',
      5 => 'Leads',
    ),
  ),
  'LBL_TABGROUP_ACTIVITIES' => 
  array (
    'label' => 'LBL_TABGROUP_ACTIVITIES',
    'modules' => 
    array (
      0 => 'Emails',
      1 => 'Calls',
      2 => 'Notes',
      3 => 'AOS_Quotes',
      4 => 'AOS_PDF_Templates',
      5 => 'AOR_Reports',
      6 => 'Bugs',
    ),
  ),
  'LBL_GROUPTAB3_1461129123' => 
  array (
    'label' => 'LBL_GROUPTAB3_1461129123',
    'modules' => 
    array (
      0 => 'AOS_Products',
      1 => 'AOS_Quotes',
      2 => 'AOS_Invoices',
      3 => 'AOS_Contracts',
      4 => 'vik_Dealership',
      5 => 'Home',
    ),
  ),
);