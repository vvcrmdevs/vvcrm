<?php
$app_list_strings['moduleList']['AOS_Products']='Vehicles';
$app_list_strings['moduleListSingular']['AOS_Products']='Vehicles';
$app_strings['LBL_GROUPTAB3_1461129123'] = 'Viking';

$GLOBALS['app_list_strings']['brand_list']=array (
  'Brand' => 'Select brand',
  'TOYOTA' => 'TOYOTA',
  'HONDA' => 'HONDA',
  'HYUNDAI' => 'HYUNDAI',
);
$GLOBALS['app_list_strings']['viking_postcode_radius_list']=array (
  20 => '20 km',
  60 => '60 km',
  120 => '120 km',
  160 => ' 160 km',
  200 => '200 km',
);

$GLOBALS['app_list_strings']['vat_list']=array (
  10 => '10%',
);
$GLOBALS['app_list_strings']['viking_customer_type_list']=array (
  'none' => 'None',
  'novated' => 'Novated',
  'fleet' => 'Fleet',
  'government' => 'Government',
);

$GLOBALS['app_list_strings']['discount_list']=array (
  'none' => 'None',
  'fleet' => 'Fleet',
  'monthly' => 'Best Monthly',
);
$app_list_strings['moduleList']['vik_vv_options']='VikingVehiclesOptions';
$app_list_strings['moduleListSingular']['vik_vv_options']='VikingVehiclesOptions';

$GLOBALS['app_list_strings']['au_states_list']=array (
  'ALL' => 'All States',
  'ACT' => 'ACT',
  'NSW' => 'NSW',
  'NT' => 'NT',
  'QLD' => 'QLD',
  'TAS' => 'TAS',
  'VIC' => 'VIC',
  'WA' => 'WA',
);
$app_list_strings['moduleList']['vik_Dealership']='Dealers';
$app_list_strings['moduleListSingular']['vik_Dealership']='Dealer';