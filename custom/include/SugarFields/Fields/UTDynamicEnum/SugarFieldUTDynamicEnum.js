if(typeof(edit_dropdown)=="undefined"){
        var  edit_dropdown =  function (field_name,option_name){
            updateDropdownProcess(field_name,option_name);
        }
        var updateDropdownDlg;
        var updateDropdownProcess = function (field_name,option_name){
            updateDropdownDlg = new YAHOO.widget.SimpleDialog("dlg",  {
                visible:false,width:"600px",height:"200px",zIndex:1000,fixedcenter:true,modal:true,draggable:false,close:false,
                buttons :[{text:"Cancel", handler:handleCancel},{text:"Save", handler:updateDropdown, isDefault:true}]
            });
        
            var top = '<form name="quick_dropdown_editor"  id="quick_dropdown_editor"><table width="400px;" height="100px;" class="edit view">'
                    + '<input type="hidden" name="quick_edit_dropdown_name" id="quick_edit_dropdown_name" value="'+option_name+'">'
                    + '<input type="hidden" name="quick_edit_dropdown_field_name" id="quick_edit_dropdown_field_name" value="'+field_name+'">'
                    
                    + '<tr><td valign="top" id=\'label_key\' width=\'50%\' align="center">Key<span class="required">*</span></td>' 
                    + '<td valign="top" id=\'label_value\' width=\'50%\' align="center">Value<span class="required">*</span></td><tr>'
                    + '<tr><td valign="top" width=\'50%\' ><input type="text" maxlength="255" size="40" id="quick_edit_dropdown_key" name="quick_edit_dropdown_key"></td>'
                    +'<td valign="top" width=\'50%\' ><input type="text" maxlength="255" size="40" id="quick_edit_dropdown_value" name="quick_edit_dropdown_value"></td></tr>'
                    
                top += '</table></form>';
            
            updateDropdownDlg.setHeader("Add DropDown Value");
            updateDropdownDlg.setBody(top);
            updateDropdownDlg.render(document.body);
            updateDropdownDlg.show();
    }
    var handleCancel = function() {
        this.cancel();
    };

    var updateDropdown = function(){
        var callback = {
            success : function(r) {
                if (r.responseText){
                    if ((r.responseText=='') || (r.responseText==null)){
                        alert("There was an error processing your request.");
                        SUGAR.ajaxUI.hideLoadingPanel();
                    } else {
                        res = eval('(' + r.responseText + ')');
                        if(res.success)
                        {
                            var ddl = YAHOO.util.Dom.getElementsByClassName(res.field_name)[0];
                            ddl.options.add(new Option(res.droddown_value,res.droddown_key));
                            ddl.value = res.droddown_key;
                        }
                        SUGAR.ajaxUI.hideLoadingPanel();
                        updateDropdownDlg.hide();
                    }
                }
            }
        };
        var dropdown_name = document.getElementById('quick_edit_dropdown_name').value;
        var dropdown_field_name = document.getElementById('quick_edit_dropdown_field_name').value;
        var dropdown_key = document.getElementById('quick_edit_dropdown_key').value;
        var dropdown_value = document.getElementById('quick_edit_dropdown_value').value;
        var postData = '&dropdown_name=' + dropdown_name;
           postData += '&dropdown_key=' + dropdown_key;
           postData += '&dropdown_value=' + dropdown_value;
           postData += '&dropdown_field_name=' + dropdown_field_name;
        addToValidate('quick_dropdown_editor', 'quick_edit_dropdown_key', 'DBName', false, "Key - Must be alphanumeric, begin with a letter and contain no spaces." );
        addToValidate('quick_dropdown_editor', 'quick_edit_dropdown_value', 'varchar', false,"Please enter a label that will be used as the Display Name for this module"); 
        
        if(check_form('quick_dropdown_editor')) {
            if(dropdown_key =='' || dropdown_value == '') {
                alert("Values are required for both the Item Name and the Display Label.");
                return false;
            }
            var dropdown = YAHOO.util.Dom.getElementsByClassName(dropdown_field_name)[0];
            if(typeof(dropdown) !== 'undefined'){
                for(var i = 0; i < dropdown.options.length; i++){
                    if(dropdown.options[i].value.toLowerCase() == dropdown_key.toLowerCase()){
                        alert("Key already exists in list");
                        return false;
                    }
                }
            }
            SUGAR.ajaxUI.showLoadingPanel();
            YAHOO.util.Connect.asyncRequest('POST', 'index.php?module=UT_DynamicDropdown&sugar_body_only=true&entryPoint=update_dropdown',callback, postData);    
        }
        else
           return false;
        };//End Update dropdwon
}//End main condition