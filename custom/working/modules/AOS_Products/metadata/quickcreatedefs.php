<?php
$module_name = 'AOS_Products';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
        'headerTpl' => 'modules/AOS_Products/tpls/EditViewHeader.tpl',
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/AOS_Products/js/products.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'dealer_c',
            'studio' => 'visible',
            'label' => 'LBL_DEALER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'part_number',
            'label' => 'LBL_PART_NUMBER',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'variant_c',
            'label' => 'LBL_VARIANT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'price',
            'label' => 'LBL_PRICE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'registration_and_ctp_c',
            'label' => 'LBL_REGISTRATION_AND_CTP',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'stock_level_c',
            'label' => 'LBL_STOCK_LEVEL',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'product_image',
            'customCode' => '{$PRODUCT_IMAGE}',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'tint_c',
            'label' => 'LBL_TINT',
          ),
          1 => 
          array (
            'name' => 'paint_c',
            'label' => 'LBL_PAINT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'mats_c',
            'label' => 'LBL_MATS',
          ),
          1 => 
          array (
            'name' => 'towbar_c',
            'label' => 'LBL_TOWBAR',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'viking_discount_c',
            'label' => 'LBL_VIKING_DISCOUNT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'monthly_bonuses_incentives_c',
            'label' => 'LBL_MONTHLY_BONUSES_INCENTIVES',
          ),
          1 => 
          array (
            'name' => 'best_monthly_discount_c',
            'label' => 'LBL_BEST_MONTHLY_DISCOUNT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'general_fleet_claim_c',
            'label' => 'LBL_GENERAL_FLEET_CLAIM',
          ),
          1 => 
          array (
            'name' => 'general_fleet_discount_c',
            'label' => 'LBL_GENERAL_FLEET_DISCOUNT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'national_fleet_rebate_c',
            'label' => 'LBL_NATIONAL_FLEET_REBATE',
          ),
          1 => 
          array (
            'name' => 'national_fleet_discount_c',
            'label' => 'LBL_NATIONAL_FLEET_DISCOUNT',
          ),
        ),
      ),
    ),
  ),
);
?>
