<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-05-14 04:40:02
$dictionary['AOS_Products']['fields']['general_fleet_claim_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['general_fleet_claim_c']['labelValue']='General Fleet Claim';

 

 // created: 2016-05-14 04:39:30
$dictionary['AOS_Products']['fields']['stamp_duty_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['stamp_duty_c']['labelValue']='Stamp Duty';

 

 // created: 2016-05-27 05:57:18
$dictionary['AOS_Products']['fields']['free_delivery_c']['inline_edit']='';
$dictionary['AOS_Products']['fields']['free_delivery_c']['labelValue']='Free Delivery';

 

 // created: 2016-05-14 04:40:49
$dictionary['AOS_Products']['fields']['stock_level_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['stock_level_c']['options']='numeric_range_search_dom';
$dictionary['AOS_Products']['fields']['stock_level_c']['labelValue']='Stock Level';
$dictionary['AOS_Products']['fields']['stock_level_c']['enable_range_search']='1';

 

 // created: 2016-05-14 04:41:14
$dictionary['AOS_Products']['fields']['best_monthly_discount_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['best_monthly_discount_c']['labelValue']='Best Monthly Discount';

 

 // created: 2016-05-30 05:33:03
$dictionary['AOS_Products']['fields']['price']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['price']['merge_filter']='disabled';

 

 // created: 2016-05-10 23:34:15
$dictionary['AOS_Products']['fields']['variant']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['variant']['comments']='not used';
$dictionary['AOS_Products']['fields']['variant']['importable']='false';
$dictionary['AOS_Products']['fields']['variant']['merge_filter']='disabled';

 

 // created: 2016-05-31 06:56:10
$dictionary['AOS_Products']['fields']['postcode_c']['inline_edit']='';
$dictionary['AOS_Products']['fields']['postcode_c']['labelValue']='Dealer Postcode';

 

 // created: 2016-05-14 04:42:15
$dictionary['AOS_Products']['fields']['paint_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['paint_c']['labelValue']='Paint';

 

 // created: 2016-04-20 00:45:39
$dictionary['AOS_Products']['fields']['variant_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['variant_c']['labelValue']='Variant';

 

 // created: 2016-05-05 00:09:52
$dictionary['AOS_Products']['fields']['price_usdollar']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['price_usdollar']['duplicate_merge']='disabled';
$dictionary['AOS_Products']['fields']['price_usdollar']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products']['fields']['price_usdollar']['merge_filter']='disabled';
$dictionary['AOS_Products']['fields']['price_usdollar']['enable_range_search']=false;

 

 // created: 2016-04-20 00:54:17
$dictionary['AOS_Products']['fields']['description']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['description']['comments']='Full text of the note';
$dictionary['AOS_Products']['fields']['description']['merge_filter']='disabled';

 

 // created: 2016-05-31 05:51:05
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['name']='delivery_distance_calc';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['vname']='LBL_DELIVERY_DISTANCE_CALC';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['type']='varchar';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['len']='16';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['source']='non-db';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['studio']='visible';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['inline_edit']='';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['importable']='false';
$dictionary['AOS_Products']['fields']['delivery_distance_calc']['merge_filter']='disabled';

 


 // created: 26/05/2016
// Author: Bill Forsyth
// non-db field used to to create customer state field which is used to calculate
//  customer distance
// modded 31/05/2016
$dictionary['AOS_Products']['fields']['customer_state']= array (
    'name' => 'customer_state',
    'vname' => 'LBL_CUST_STATE_LKP',
    'type' => 'varchar',
    'len' => '24',
    'studio' => 'visible',    
    'source' => 'non-db',
);



 // created: 2016-04-26 05:46:46
$dictionary['AOS_Products']['fields']['vik_dealership_id_c']['inline_edit']=1;

 

 // created: 2016-05-14 04:42:50
$dictionary['AOS_Products']['fields']['general_fleet_discount_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['general_fleet_discount_c']['labelValue']='General Fleet Discount';

 

 // created: 2016-05-14 04:43:15
$dictionary['AOS_Products']['fields']['towbar_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['towbar_c']['labelValue']='Towbar';

 

 // created: 2016-05-14 04:43:33
$dictionary['AOS_Products']['fields']['mats_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['mats_c']['labelValue']='Mats';

 

// created: 2016-04-20 02:26:48
$dictionary["AOS_Products"]["fields"]["vik_dealership_aos_products_1"] = array (
  'name' => 'vik_dealership_aos_products_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_products_1',
  'source' => 'non-db',
  'module' => 'vik_Dealership',
  'bean_name' => 'vik_Dealership',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_PRODUCTS_1_FROM_VIK_DEALERSHIP_TITLE',
  'id_name' => 'vik_dealership_aos_products_1vik_dealership_ida',
);
$dictionary["AOS_Products"]["fields"]["vik_dealership_aos_products_1_name"] = array (
  'name' => 'vik_dealership_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_PRODUCTS_1_FROM_VIK_DEALERSHIP_TITLE',
  'save' => true,
  'id_name' => 'vik_dealership_aos_products_1vik_dealership_ida',
  'link' => 'vik_dealership_aos_products_1',
  'table' => 'vik_dealership',
  'module' => 'vik_Dealership',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["vik_dealership_aos_products_1vik_dealership_ida"] = array (
  'name' => 'vik_dealership_aos_products_1vik_dealership_ida',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


 // created: 2016-05-30 05:33:30
$dictionary['AOS_Products']['fields']['monthly_bonuses_incentives_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['monthly_bonuses_incentives_c']['labelValue']='Monthly Bonuses';

 

 // created: 2016-05-14 04:44:43
$dictionary['AOS_Products']['fields']['registration_and_ctp_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['registration_and_ctp_c']['labelValue']='Rego + CTP';

 

 // created: 2016-05-14 04:45:10
$dictionary['AOS_Products']['fields']['predelivery_c']['inline_edit']='';
$dictionary['AOS_Products']['fields']['predelivery_c']['labelValue']='Pre-Delivery';

 

 // created: 2016-05-16 23:39:38
$dictionary['AOS_Products']['fields']['fuel_economy_combined_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['fuel_economy_combined_c']['labelValue']='Fuel Economy Combined';

 

 // created: 2016-05-14 04:39:03
$dictionary['AOS_Products']['fields']['viking_discount_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['viking_discount_c']['labelValue']='Viking Discount';

 

 // created: 2016-05-14 04:38:36
$dictionary['AOS_Products']['fields']['national_fleet_discount_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['national_fleet_discount_c']['labelValue']='National Fleet Discount';

 


 // created: 26/05/2016
// Author: Bill Forsyth
// non-db field used to to create customer suburb field which is used to calculate
//  customer distance
// modded 31/05/2016
$dictionary['AOS_Products']['fields']['customer_suburb']= array (
    'name' => 'customer_suburb',
    'vname' => 'LBL_CUST_SUBURBS_LKP',
    'type' => 'varchar',
    'len' => '48',
    'studio' => 'visible',    
    'source' => 'non-db',
);



 // created: 2016-04-26 05:46:46
$dictionary['AOS_Products']['fields']['dealer_c']['inline_edit']='';
$dictionary['AOS_Products']['fields']['dealer_c']['labelValue']='Motor Dealer';

 

 // created: 2016-05-18 23:34:13
$dictionary['AOS_Products']['fields']['engine_cylinders_c']['inline_edit']='';
$dictionary['AOS_Products']['fields']['engine_cylinders_c']['labelValue']='Engine Cylinders';

 

 // created: 2016-05-27 04:01:37
$dictionary['AOS_Products']['fields']['state_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['state_c']['labelValue']='State';

 

 // created: 2016-05-14 04:47:15
$dictionary['AOS_Products']['fields']['monthly_bonus_gold_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['monthly_bonus_gold_c']['labelValue']='Monthly Bonus Gold';

 

 // created: 2016-05-16 00:37:35
$dictionary['AOS_Products']['fields']['hybrid_or_electric_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['hybrid_or_electric_c']['labelValue']='Hybrid or Electric';

 

 // created: 2016-05-16 00:27:00
$dictionary['AOS_Products']['fields']['other_accessories_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['other_accessories_c']['labelValue']='Other Accessories';

 

 // created: 2016-05-14 04:48:15
$dictionary['AOS_Products']['fields']['luxury_car_tax_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['luxury_car_tax_c']['labelValue']='Luxury Car Tax';

 

 // created: 2016-05-03 05:11:31
$dictionary['AOS_Products']['fields']['select_paint_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['select_paint_c']['labelValue']='Select Paint';

 

 // created: 2016-05-14 04:48:35
$dictionary['AOS_Products']['fields']['national_fleet_rebate_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['national_fleet_rebate_c']['labelValue']='National Fleet Rebate';

 

 // created: 2016-05-14 04:48:54
$dictionary['AOS_Products']['fields']['tint_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['tint_c']['labelValue']='Tint';

 

 // created: 2016-04-20 00:42:43
$dictionary['AOS_Products']['fields']['name']['len']='48';
$dictionary['AOS_Products']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Products']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Products']['fields']['name']['unified_search']=false;

 

 // created: 2016-04-20 00:47:51
$dictionary['AOS_Products']['fields']['part_number']['len']='48';
$dictionary['AOS_Products']['fields']['part_number']['required']=true;
$dictionary['AOS_Products']['fields']['part_number']['inline_edit']='';
$dictionary['AOS_Products']['fields']['part_number']['merge_filter']='disabled';

 
?>