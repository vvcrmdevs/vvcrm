<?php
// @Author: Bill Forsyth
// @Project: Viking Vehicles project
// @Date: 04/04/2016, 31/05/2016
// @description: Add related (dealership module) data to product (vehicle) 
//                  listview 

class dealershipInfo {
      function updateVehicle(&$bean, $event, $arguments) 
      {
           static $cstate = ''; 
           static $csuburb =  ''; 
           static $dstate = ''; 
           static $dsuburb =  '';
           static $fdelivery = '';
           //
           $product_id = $bean->id;
           $dealer_id = $bean->vik_dealership_id_c;           
         
           if (isset($dealer_id)) {
                $query =  "SELECT postcode, predelivery, city, state, free_delivery_distance_c FROM vik_dealership ";
                $query .= " inner join vik_dealership_cstm ON id = id_c";
                $query .= " WHERE length(postcode)>0";
                $query .= " and id = '$dealer_id' LIMIT 1";
                //
                // Get dealership postcode and pre-delivery charges
                $results = $bean->db->query($query, true);
                //
                //
                if ( $row = $bean->db->fetchByAssoc($results) ) {
                    
                    $dealer_postcode = $row['postcode'];
                    $dealer_pre_delivery = $row['predelivery'];
                    //
                    // A drop of prevention:
                    // Make sure State string is in uppercase abbreviated format:
                    $dealer_state = strtoupper($row['state']);
                    $test_state  = substr($dealer_state,0,4); 
                    switch ($test_state){
                        case 'VICT': $dealer_state = 'VIC'; break;
                        case 'QUEE': $dealer_state = 'QLD'; break;
                        case 'TASM': $dealer_state = 'TAS'; break;
                        case 'SOUT': $dealer_state = 'SA';  break;
                        case 'NORT': $dealer_state = 'NT';  break;
                        case 'WEST': $dealer_state = 'WA';  break;
                        case 'AUST': $dealer_state = 'ACT'; break;
                        default: break;
                    }
                    
                    $dealer_suburb = $row['city'];
                    $free_delivery_km = $row['free_delivery_distance_c'];
                    //////////////////////////////////////////////
                    
                    if (isset($GLOBALS['customer_state']) && isset($GLOBALS['customer_suburb'])) {
                    
                        $customer_state = $GLOBALS['customer_state'];
                        $customer_suburb = $GLOBALS['customer_suburb'];
                        
                        if (strlen($customer_suburb)>0 && strlen($customer_state)>0) {
                            if ( $dstate !== $dealer_state ||
                                    $dsuburb !== $dealer_suburb ||
                                    $cstate !== $customer_state ||
                                    $csuburb !== $customer_suburb ) {
                                $q = 'SELECT postcode, suburb, radius, MAX(kilometers) as KM,'
                                    . "IF( radius > kilometers, 'free delivery', 'charges apply' ) as `delivery`  FROM (";

                                 //- The distance along the surface of the (spherical) earth between two arbitrary points,
                                 // -- in degrees, given by their latitude and longitude in= degrees?  
                                 // -- That’s determined by the Spherical Cosine Law, or the Haversine Formula

                                $q .= 'SELECT pc.postcode, pc.suburb, pc.state, pc.lat, pc.lon,p.radius,'
                                   . ' p.distance_unit * DEGREES(ACOS(COS(RADIANS(p.latpoint))'
                                   . '  * COS(RADIANS(pc.lat))'
                                   . '  * COS(RADIANS(p.longpoint - pc.lon))'
                                   . '  + SIN(RADIANS(p.latpoint))'
                                   . '  * SIN(RADIANS(pc.lat)))) AS kilometers'
                                   . ' FROM vik_postcode AS pc'
                                   . ' JOIN ( /* these are the query parameters */'
                                   . ' SELECT  lat  AS latpoint,  lon AS longpoint,'
                                   .  $free_delivery_km . ' AS radius, 111.045 AS distance_unit';

                                $q  .= " from vik_postcode where suburb = '" . $dealer_suburb . "' and state = '" .$dealer_state . "'"
                                     . ') AS p ON 1=1'
                                     . ') AS d'
                                     . " WHERE suburb = '" . $customer_suburb . "' AND state = '" . $customer_state ."';";

                                //////////////////////////////////////////
    //                            $sql1 = "CALL town_to_town('"
    //                                    . $customer_suburb . "','" . $customer_state . "','"
    //                                    . $dealer_suburb . "','" . $dealer_state . "',"
    //                                    . $free_delivery_km 
    //                                    . ");";

                                $results = $bean->db->query($q);
                                if ( $row = $bean->db->fetchByAssoc($results) ) {
                                    $fdelivery = $row['delivery'];
                                    $fdelivery .= " [" . number_format((float)$row['KM'], 2, '.', '')  . "Km]";
                                    
                                }  
                            }  
                        } 
                        $dstate = $dealer_state ;
                        $dsuburb = $dealer_suburb;
                        $cstate =$customer_state;
                        $csuburb = $customer_suburb;                         
                            
                    } else { 
                        $fdelivery = '?';                        
                    }
                                  
                    
                    $bean->delivery_distance_calc = $fdelivery;
                    $bean->state_c = $dealer_state;
                    $bean->predelivery =  $dealer_pre_delivery;
                    $bean->postcode_c = $dealer_postcode;
                                     
                    ////////////////////////////////////////////////////
                    $sql = "update aos_products_cstm "                            
                        .  " set postcode_c = '" . $dealer_postcode . "'"
                        .  ", predelivery_c = " . $dealer_pre_delivery 
                        .  ", state_c = '" . $dealer_state . "'"    
                        .  " where id_c = '" . $product_id . "';" ;
                    // Set correct values for the dealership data                
                    $bean->db->query($sql);
                }  
            } 
      }
}


?>
