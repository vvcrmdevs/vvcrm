<?php
// @Package VVCRM
// @Author: Bill forsyth 
// @Date: 12/04/2016
// @description:  overrides ViewPopup.display() function to
//                  inject javascript into AOS_Products popup page,
//                   to suppport cascading dropdowns on vehicle search popup page
require_once('include/MVC/View/views/view.popup.php'); 

class CustomAOS_ProductsViewPopup extends ViewPopup
{
     // bill f
    //  custom listviw menu option "quote on vehicles"
    //
    public function preDisplay()
    {
        // add Viking Vehicle view list button action 
        $js = <<<EOQ
                    
<script type="text/javascript" src="custom/modules/AOS_Products/js/jquery.js"></script>
<script type="text/javascript">
      
// Fix up multiselects to appear as proper dropdowns
$("#name_advanced").replaceWith('<select id="name_advanced" name="name_advanced[]" style="width: 150px"></select>');        
           
$("#part_number_advanced").replaceWith('<select id="part_number_advanced" name="part_number_advanced[]" style="width: 150px"></select>');        
   
$("#variant_c_advanced").replaceWith('<select id="variant_c_advanced" name="variant_c_advanced[]" style="width: 150px"></select></td></tr><tr><td>');        
   
$("#customer_state_advanced").replaceWith('<select id="customer_state_advanced" name="customer_state_advanced[]"  style="width: 150px"></select>');        
   
$("#customer_suburb_advanced").replaceWith('<select id="customer_suburb_advanced" name="customer_suburb_advanced[]"  style="width: 150px"></select>');        
</script>
EOQ;
         parent::preDisplay();
         echo $js;
//        if(!(ACLController::checkAccess('AOS_Quotes', 'edit', true))){
//            ACLController::displayNoAccess();
//            die;
//        } else {
//            $this->lv->actionsMenuExtraItems[] = $this->buildMyMenuItem();
//        }
        // //<script type="text/javascript" src="custom/modules/AOS_Products/js/jquery.js"></script>
    }
    
    public function display(){
$js = <<<EOQ
 
      
    <script type="text/javascript">
     
    // Fix up multiselects to appear as proper dropdowns
    var name_node = document.getElementById('name_advanced');
    name_node.removeAttribute('multiple');
    name_node.removeAttribute('size');

    var part_node = document.getElementById('part_number_advanced');
    part_node.removeAttribute('multiple');
    part_node.removeAttribute('size');

    var variant_node = document.getElementById('variant_c_advanced');
    variant_node.removeAttribute('multiple');
    variant_node.removeAttribute('size');
        
    // Hide the cost field and label  -- used as filler, to force state and suburb dropdowns onto single line 
    $('label[for="cost_advanced"]').hide ();
    var advsearch = document.getElementById('cost_advanced');
    advsearch.style.visibility = "hidden";
        
        
    var state_node = document.getElementById('customer_state_advanced');
    state_node.removeAttribute('multiple');
    state_node.removeAttribute('size');
    
    var suburb_node = document.getElementById('customer_suburb_advanced');
    suburb_node.removeAttribute('multiple');
   suburb_node.removeAttribute('size');
   
    // Remove search on calculated field, as there is no corresponding database column to sort on
    // and the list of cars disappears till the 'clear' and 'search' buttons are pressed in sequence.
    var elements = document.getElementsByClassName('listViewThLinkS1');
    for (var i=0; i<elements.length; i++) {     
        if(elements[i].textContent.indexOf("Delivery Charges") > -1){         
           elements[i].setAttribute('href','#');
           elements[i].setAttribute('onclick','return false;');
        }
    }   
        
        
        
    function get_models(brand ) {
          if(brand){                     
            $.ajax({
              type:'POST',
                url:'custom/modules/AOS_Products/load_models.php',
                data:'brand='+brand,
                success:function(html){
	   	    $('#part_number_advanced').html(html);
                    localStorage.setItem("pmodels", html);
                    
                    var shtml = '<option value="">All variants</option>';
                    $('#variant_c_advanced').html(shtml); 
                    localStorage.setItem("pvariant", shtml);
                    localStorage.setItem("pselected_variant", null);
                }
            });
            
        }else{
            var shtml = '<option value="">All models</option>';
        
	    $('#part_number_advanced').html(shtml);
            localStorage.setItem("pmodels", shtml);
            localStorage.setItem("pselected_model", null);
            
            shtml = '<option value="">All variants</option>';
        
            $('#variant_c_advanced').html(shtml); 
            localStorage.setItem("pvariant", shtml);
            localStorage.setItem("pselected_variant", null)        
        }
     }
    
   $('#name_advanced').change(function(){
       var brand = $(this).val();
       get_models(brand );
    });
    
   $('#name_advanced').load(function(){
       var brand = $(this).val();
       get_models(brand );
    });
        
    $('#name_advanced').click(function(){
       var brand = $(this).val();
       get_models(brand );
    });
     
        
    function get_variants(brand, model) {
         if(model && brand){
            localStorage.setItem("pselected_model", model);
              
            $.ajax({
                type:'POST',
                url:'custom/modules/AOS_Products/load_variants.php',
                data:'brand='+brand + '&model='+model,
                success:function(html){
		    $('#variant_c_advanced').html(html);
                    localStorage.setItem("pvariant", html);
                }
            });
        }else{
            var shtml = '<option value="">All variants</option>';
        
            $('#variant_c_advanced').html(shtml); 
            localStorage.setItem("pvariant", shtml);
            localStorage.setItem("pselected_variant", null);
        }
    }
    
    // Remember selected model variant
    $('#variant_c_advanced').change(function(){ 
        var variant = $(this).val(); 
        localStorage.setItem("pselected_variant", variant);
    });
        
    $('#variant_c_advanced').load(function(){ 
        var variant = $(this).val(); 
        localStorage.setItem("pselected_variant", variant);
    });
        
    $('#variant_c_advanced').click(function(){ 
        var variant = $(this).val(); 
        localStorage.setItem("pselected_variant", variant);
    });
        
    $('#part_number_advanced').load(function(){ 
        var vbrand = $('#name_advanced').val();
        var vmodel = $('#part_number_advanced').val();
                
        get_variants(vbrand,vmodel);      
    });   
 
    $('#part_number_advanced').change(function(){ 
        var vbrand = $('#name_advanced').val();
        var vmodel = $('#part_number_advanced').val();
                
        get_variants(vbrand,vmodel);        
    });
        
    $('#part_number_advanced').click(function(){ 
        var vbrand = $('#name_advanced').val();
        var vmodel = $('#part_number_advanced').val();
                
        get_variants(vbrand,vmodel);        
    });    
     
        
    function get_suburbs(state) {
         if(state){
            $.ajax({
                type:'POST',
                url:'custom/modules/AOS_Products/load_suburbs.php',
                data:'state='+state,
                success:function(html){
                   $('#customer_suburb_advanced').html(html); 
                   localStorage.setItem("psuburbs", html);
                }
            });
        }else{
            var shtml = '<option value="">All Suburbs</option>';
            $('#customer_suburb_advanced').html(shtml); 
            localStorage.setItem("psuburbs", shtml);
            localStorage.setItem("pselected_suburb", null);
        }
    }
        
    // remember selected suburb
    $('#customer_suburb_advanced').change(function(){ 
        var burb = $(this).val(); 
        
        localStorage.setItem("pselected_suburb", burb);
    });
        
    $('#customer_state_advanced').change(function(){ 
      var state = $(this).val();
      get_suburbs(state);
    });
        
    $('#customer_state_advanced').load(function(){ 
       var state = $(this).val();
       get_suburbs(state);
    });
        
    $('#customer_state_advanced').click(function(){ 
       var state = $(this).val();
       get_suburbs(state);
    }); 
    
    // Function sets dropdown list selection to desired element
    function setSelectedValue(selectObj, valueToSet) {
        for (var i = 0; i < selectObj.options.length; i++) {
            if (selectObj.options[i].text== valueToSet) {
                selectObj.options[i].selected = true;
               return;
           }
        }
    }    
        
    window.onload = function() {
        //alert("reloading form data");
        //
        // Restore dropdown lists
        var html = localStorage.getItem("psuburbs");
        if (html != 'null' || html !== null ) 
           $('#customer_suburb_advanced').html(html);
                
        var html = localStorage.getItem("pmodels");
        if (html != 'null'|| html !== null ) 
           $('#part_number_advanced').html(html);
        
        var html = localStorage.getItem("pvariant");
        if (html != 'null' || html !== null ) 
           $('#variant_c_advanced').html(html);  
        
        // Restore selected values
        var val = localStorage.getItem('pselected_model');
        if (val != 'null' || html !== null ) {
           var objsel = document.getElementById("part_number_advanced");
           setSelectedValue(objsel, val);
        }
        //
        var val = localStorage.getItem('pselected_variant');
        if (val != 'null' || html !== null ) {
            var objsel =  document.getElementById("variant_c_advanced");  
            setSelectedValue(objsel, val);
        }
        //
        var val = localStorage.getItem('pselected_suburb');
        if (val != 'null' || html !== null ) {
           var objsel =  document.getElementById("customer_suburb_advanced"); 
           setSelectedValue(objsel, val);
        }
    }

  
</script>
EOQ;

        parent::display();
        echo $js;

    }
  
 
}

?>
