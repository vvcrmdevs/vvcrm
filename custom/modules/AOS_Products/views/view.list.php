<?php
//
// @Author: Bill forsyth 
// @Date: 12/04/2016
// @description:  overrides ViewList.display() function to
// inject javascript into AOS_Products page,
//   to suppport cascading dropdowns on vehicle search page
require_once('include/MVC/View/views/view.list.php');

class CustomAOS_ProductsViewList extends ViewList
{
    public function test_js(){
        $js = <<<EOQ
            <script language='javascript'>
                // display prompt box that ask for name and 
// store result in a variable called who
var who = window.prompt("What is your name");

// display prompt box that ask for favorite color and 
// store result in a variable called favcolor
var favcolor = window.prompt("What is your favorite color");

// write "Hello" followed by person' name to browser window
document.write("Hello " + who);

// Change background color to their favorite color
document.bgColor = favcolor; </script>
EOQ;
    }
    
     public function display(){
$js = <<<EOQ
 
    <script type="text/javascript" src="custom/modules/AOS_Products/js/jquery.js"></script>
    
    <script type="text/javascript">
    // Fix up multiselects to appear as proper dropdowns
    var name_node = document.getElementById('name_basic');
    name_node.removeAttribute('multiple');
    name_node.removeAttribute('size');

    var part_node = document.getElementById('part_number_basic');
    part_node.removeAttribute('multiple');
    part_node.removeAttribute('size');

    var variant_node = document.getElementById('variant_c_basic');
    variant_node.removeAttribute('multiple');
    variant_node.removeAttribute('size');

    var pc_node = document.getElementById('customer_state_basic');
    pc_node.removeAttribute('multiple');
    pc_node.removeAttribute('size');
    
    var radius_node = document.getElementById('customer_suburb_basic');
    radius_node.removeAttribute('multiple');
    radius_node.removeAttribute('size');
        
    // Hide the advanced search link   
    var advsearch = document.getElementById('advanced_search_link');
    advsearch.style.visibility = "hidden";
       
    // Remove search on calculated field, as there is no corresponding database column to sort on
    // and the list of cars disappears till the 'clear' and 'search' buttons are pressed in sequence.
    var elements = document.getElementsByClassName('listViewThLinkS1');
     for (var i=0; i<elements.length; i++) {     
      if(elements[i].textContent.indexOf("Delivery Charges") > -1){         
          elements[i].setAttribute('href','#');
          elements[i].setAttribute('onclick','return false;');
        }
     }
    
    $("Delivery Charges").removeAttr("href");
     
    function get_models(brand ) {
          if(brand){                     
            $.ajax({
              type:'POST',
                url:'custom/modules/AOS_Products/load_models.php',
                data:'brand='+brand,
                success:function(html){
	   	    $('#part_number_basic').html(html);
                    localStorage.setItem("models", html);
                    
                    var shtml = '<option value="">All variants</option>';
                    $('#variant_c_basic').html(shtml); 
                    localStorage.setItem("variant", shtml);
                    localStorage.setItem("selected_variant", null);
                }
            });
            
        }else{
            var shtml = '<option value="">All models</option>';
	    $('#part_number_basic').html(shtml);
            localStorage.setItem("models", shtml);
            localStorage.setItem("selected_model", null);
            
        
            shtml = '<option value="">All variants</option>';
            $('#variant_c_basic').html(shtml); 
            localStorage.setItem("variant", shtml);
            localStorage.setItem("selected_variant", null)        
        }
    }
    
   $('#name_basic').change(function(){
       var brand = $(this).val();
       get_models(brand );
    });
    
   $('#name_basic').load(function(){
       var brand = $(this).val();
       get_models(brand );
    });
        
    $('#name_basic').click(function(){
       var brand = $(this).val();
       get_models(brand );
    });
     
        
    function get_variants(brand, model) {
         if(model && brand){
            $.ajax({
                type:'POST',
                url:'custom/modules/AOS_Products/load_variants.php',
                data:'brand='+brand + '&model='+model,
                success:function(html){
		    $('#variant_c_basic').html(html);
                    localStorage.setItem("variant", html);
                      
                }
            });
            localStorage.setItem("selected_model", model);
        }else{
            var shtml = '<option value="">All variants</option>';
            $('#variant_c_basic').html(shtml); 
            localStorage.setItem("variant", shtml);
            localStorage.setItem("selected_variant", null);
        }
    }
    
    // Remember selected model variant
    $('#variant_c_basic').change(function(){ 
        var variant = $(this).val(); 
        localStorage.setItem("selected_variant", variant);
    });
        
    $('#variant_c_basic').load(function(){ 
        var variant = $(this).val(); 
        localStorage.setItem("selected_variant", variant);
    });
        
    $('#variant_c_basic').click(function(){ 
        var variant = $(this).val(); 
        localStorage.setItem("selected_variant", variant);
    });
        
    $('#part_number_basic').load(function(){ 
        var vbrand = $('#name_basic').val();
        var vmodel = $('#part_number_basic').val();
                
        get_variants(vbrand,vmodel);      
    });   
 
    $('#part_number_basic').change(function(){ 
        var vbrand = $('#name_basic').val();
        var vmodel = $('#part_number_basic').val();
                
        get_variants(vbrand,vmodel);        
    });
        
    $('#part_number_basic').click(function(){ 
        var vbrand = $('#name_basic').val();
        var vmodel = $('#part_number_basic').val();
                
        get_variants(vbrand,vmodel);        
    });    
     
        
    function get_suburbs(state) {
         if(state){
            $.ajax({
                type:'POST',
                url:'custom/modules/AOS_Products/load_suburbs.php',
                data:'state='+state,
                success:function(html){
                   $('#customer_suburb_basic').html(html); 
                   localStorage.setItem("suburbs", html);
                }
            });
        }else{
            var shtml = '<option value="">All Suburbs</option>';
            $('#customer_suburb_basic').html(shtml); 
            localStorage.setItem("suburbs", shtml);
            localStorage.setItem("selected_suburb", null);
        }
    }
        
    // remember selected suburb
    $('#customer_suburb_basic').change(function(){ 
        var burb = $(this).val(); 
        
        localStorage.setItem("selected_suburb", burb);
    });
        
    $('#customer_state_basic').change(function(){ 
      var state = $(this).val();
      get_suburbs(state);
    });
        
    $('#customer_state_basic').load(function(){ 
       var state = $(this).val();
       get_suburbs(state);
    });
        
    $('#customer_state_basic').click(function(){ 
       var state = $(this).val();
       get_suburbs(state);
    }); 
    
    function setSelectedValue(selectObj, valueToSet) {
        for (var i = 0; i < selectObj.options.length; i++) {
            if (selectObj.options[i].text== valueToSet) {
                selectObj.options[i].selected = true;
               return;
           }
        }
    }    
        
    window.onload = function() {
       // alert("reloading form data");
        
        
               
        var html = localStorage.getItem("models");
        if (html != 'null' || html !== null ) 
           $('#part_number_basic').html(html);
        
         var val = localStorage.getItem('selected_model');
        if (val != 'null' || html !== null ) {
           var objsel = document.getElementById("part_number_basic");
           setSelectedValue(objsel, val);
        }
      //  alert("loading vehicle variants");
        var html = localStorage.getItem("variant");
        if (html !== 'null' || html !== null ) {
           $('#variant_c_basic').html(html);  
        
        }
        
        
        var val = localStorage.getItem('selected_variant');
        if (val != 'null' || html !== null ) {
            var objsel =  document.getElementById("variant_c_basic");  
            setSelectedValue(objsel, val);
        }
        
       
        var html = localStorage.getItem("suburbs");
        if (html != 'null' || html !== null ) 
           $('#customer_suburb_basic').html(html);
        
        var val = localStorage.getItem('selected_suburb');
        if (val != 'null' || html !== null ) {
           var objsel =  document.getElementById("customer_suburb_basic"); 
           setSelectedValue(objsel, val);
        }
       
    }

  
</script>
EOQ;

        parent::display();
        echo $js;

	//do_something_else();
    
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    
    
    // bill f
    //  custom listviw menu option "quote on vehicles"
    //
    public function preDisplay()
    {
        // add Viking Vehicle view list button action 
        parent::preDisplay();
        
        if(!(ACLController::checkAccess('AOS_Quotes', 'edit', true))){
            ACLController::displayNoAccess();
            die;
        } else {
            $this->lv->actionsMenuExtraItems[] = $this->buildMyMenuItem();
        }
    }
    
// Note: on language strings
// First off we add the string for the menu item text. 
// In this case “New action”. 
// We place this into `custom/Extension/modules/AOS_Products/Ext/Language/en_us.CustomMenuAction.php` 
// 
// (the `CustomMenuAction` part can be anything you like.
                  
   // protected function buildMyMenuItem()
   // {
    //    global $app_strings;        
    //    return "<a class='menuItem' style='width: 100px;' href='#' onmouseover='hiliteItem(this,\"yes\");' onmouseout='unhiliteItem(this);' "
    //    . "onclick=\"sugarListView.get_checks(); if (sugarListView.get_checks_count()==0) { alert('{$app_strings['LBL_LISTVIEW_NO_SELECTED']}'); "
    //    . "return false; } "
    //    . "document.MassUpdate.action.value='action_autoquote'; document.MassUpdate.submit();\">Quote on vehicles</a>";
   // }
    
    /**
     * @return menu item string HTML
     */
    protected function buildMyMenuItem()
    {
        global $app_strings;        
        return "<a class='menuItem' style='width: 100px;' href='#' onmouseover='hiliteItem(this,\"yes\");' onmouseout='unhiliteItem(this);' onclick=\"sugarListView.get_checks(); if (sugarListView.get_checks_count()==0) { alert('{$app_strings['LBL_LISTVIEW_NO_SELECTED']}'); return false; } document.MassUpdate.action.value='make_quote'; document.MassUpdate.submit();\">Quote</a>";
    }
 
}

?>
