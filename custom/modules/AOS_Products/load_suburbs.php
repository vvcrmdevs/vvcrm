<?php
// @Author: Bill Forsyth
// @Package VVCRM
// Cascading dropdown for vehicle search assist func
// 
define('__ROOT__', dirname(dirname(dirname(dirname(__FILE__))))); 
require_once(__ROOT__.'/config.php'); 

$svr = $sugar_config[dbconfig]['db_host_name'];
$usr =  $sugar_config[dbconfig]['db_user_name'];
$pwd = $sugar_config[dbconfig]['db_password'];
$db = $sugar_config[dbconfig]['db_name'];

$state = $_POST['state'];

if (isset($state)) {
    mysql_connect($svr, $usr, $pwd);
    mysql_select_db($db);    
    $sql = "SELECT DISTINCT suburb, suburb FROM vik_postcode"            
            . " where state = '" . strtoupper($state) . "' order by suburb asc;";      
   $query = mysql_query($sql);
   echo "<option value=''>All Suburbs</option>";
   
    while($row = mysql_fetch_row($query)) {
        echo "<option value='$row[0]'>$row[1]</option>";
    }
    mysql_close();
}

?>

