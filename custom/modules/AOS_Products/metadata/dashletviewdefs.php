<?php
$dashletData['AOS_ProductsDashlet']['searchFields'] = array (
  'name' => 
  array (
    'default' => '',
  ),
  'part_number' => 
  array (
    'default' => '',
  ),
  'variant_c' => 
  array (
    'default' => '',
  ),
);
$dashletData['AOS_ProductsDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'part_number' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PART_NUMBER',
    'width' => '10%',
    'default' => true,
    'name' => 'part_number',
  ),
  'variant_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_VARIANT',
    'width' => '25%',
    'name' => 'variant_c',
  ),
  'registration_and_ctp_c' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_REGISTRATION_AND_CTP',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'registration_and_ctp_c',
  ),
  'viking_discount_c' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_VIKING_DISCOUNT',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'viking_discount_c',
  ),
  'price' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => '5%',
    'default' => false,
    'name' => 'price',
  ),
  'stock_level_c' => 
  array (
    'type' => 'int',
    'default' => false,
    'label' => 'LBL_STOCK_LEVEL',
    'width' => '5%',
    'name' => 'stock_level_c',
  ),
  'best_monthly_discount_c' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_BEST_MONTHLY_DISCOUNT',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'best_monthly_discount_c',
  ),
  'monthly_bonuses_incentives_c' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_MONTHLY_BONUSES_INCENTIVES',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'monthly_bonuses_incentives_c',
  ),
  'national_fleet_discount_c' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_NATIONAL_FLEET_DISCOUNT',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'national_fleet_discount_c',
  ),
  'national_fleet_rebate_c' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_NATIONAL_FLEET_REBATE',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'national_fleet_rebate_c',
  ),
  'general_fleet_discount_c' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_GENERAL_FLEET_DISCOUNT',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'general_fleet_discount_c',
  ),
  'general_fleet_claim_c' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_GENERAL_FLEET_CLAIM',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'general_fleet_claim_c',
  ),
);
