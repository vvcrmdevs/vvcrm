<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Products',
    'varName' => 'AOS_Products',
    'orderBy' => 'aos_products.name',
    'whereClauses' => array (
  'name' => 'aos_products.name',
  'part_number' => 'aos_products.part_number',
  'variant_c' => 'aos_products_cstm.variant_c',
  'customer_state' => 'aos_products.customer_state',
  'customer_suburb' => 'aos_products.customer_suburb',
  'cost' => 'aos_products.cost',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'part_number',
  8 => 'variant_c',
  10 => 'customer_state',
  11 => 'customer_suburb',
  12 => 'cost',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'enum',
    'name' => 'name',
    'width' => '10%',
    'function' => 'get_brands',
  ),
  'part_number' => 
  array (
    'type' => 'enum',
    'name' => 'part_number',
    'width' => '10%',
  ),
  'variant_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_VARIANT',
    'width' => '10%',
    'name' => 'variant_c',
  ),
  'cost' => 
  array (
    'name' => 'cost',
    'width' => '10%',
  ),
  'customer_state' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_CUST_STATE_LKP',
    'width' => '10%',
    'name' => 'customer_state',
    'function' => 'get_states',
  ),
  'customer_suburb' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_CUST_SUBURBS_LKP',
    'width' => '10%',
    'name' => 'customer_suburb',
  ),
),
    'listviewdefs' => array (
  'DEALER_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_DEALER',
    'id' => 'VIK_DEALERSHIP_ID_C',
    'link' => true,
    'width' => '5%',
    'name' => 'dealer_c',
  ),
  'NAME' => 
  array (
    'width' => '5%',
    'label' => 'LBL_NAME',
    'default' => true,
    'name' => 'name',
  ),
  'PART_NUMBER' => 
  array (
    'width' => '5%',
    'label' => 'LBL_PART_NUMBER',
    'default' => true,
    'link' => true,
    'name' => 'part_number',
  ),
  'VARIANT_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_VARIANT',
    'width' => '20%',
    'name' => 'variant_c',
    'link' => true,
  ),
  'PRICE' => 
  array (
    'width' => '5%',
    'label' => 'LBL_PRICE',
    'default' => true,
    'name' => 'price',
  ),
  'DELIVERY_DISTANCE_CALC' => 
  array (
    'type' => 'varchar',
    'studio' => 'visible',
    'label' => 'LBL_DELIVERY_DISTANCE_CALC',
    'width' => '10%',
    'default' => true,
    'name' => 'delivery_distance_calc',
  ),
  'POSTCODE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_POSTCODE',
    'width' => '10%',
    'name' => 'postcode_c',
  ),
),
);
