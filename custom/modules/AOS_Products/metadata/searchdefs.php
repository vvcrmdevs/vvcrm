<?php
$module_name = 'AOS_Products';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'type' => 'enum',
        'name' => 'name',
        'default' => true,
        'width' => '10%',
        'function' => 'get_brands',
      ),
      'part_number' => 
      array (
        'type' => 'enum',
        'name' => 'part_number',
        'default' => true,
        'width' => '10%',
      ),
      'variant_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'label' => 'LBL_VARIANT',
        'width' => '10%',
        'name' => 'variant_c',
      ),
      'customer_state' => 
      array (
        'type' => 'enum',
        'default' => true,
        'label' => 'LBL_CUST_STATE_LKP',
        'width' => '10%',
        'name' => 'customer_state',
        'function' => 'get_states',
      ),
        
      'customer_suburb' => 
      array (
        'type' => 'enum',
        'default' => true,
        'label' => 'LBL_CUST_SUBURBS_LKP',
        'studio' => 'visible',
        'width' => '10%',
        'name' => 'customer_suburb',
        //'function' => 'get_suburbs',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'part_number' => 
      array (
        'name' => 'part_number',
        'default' => true,
        'width' => '10%',
      ),
      'cost' => 
      array (
        'name' => 'cost',
        'default' => true,
        'width' => '10%',
      ),
      'price' => 
      array (
        'name' => 'price',
        'default' => true,
        'width' => '10%',
      ),
      'created_by' => 
      array (
        'name' => 'created_by',
        'label' => 'LBL_CREATED',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
