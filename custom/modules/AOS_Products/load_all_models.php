<?php

// @Author: James Buchanan
// 
// This is called using AJAX to just return all the makes
// of vehicales to populate the first drop-down box, so
// the user can select Toyota, Hyundai, etc. then model
// and variant from other drop-downs.
//
// Based on load_models.php
// 
define('__ROOT__', dirname(dirname(dirname(dirname(__FILE__))))); 
require_once(__ROOT__.'/config.php'); 

$svr = $sugar_config[dbconfig]['db_host_name'];
$usr =  $sugar_config[dbconfig]['db_user_name'];
$pwd = $sugar_config[dbconfig]['db_password'];
$db = $sugar_config[dbconfig]['db_name'];

mysql_connect($svr, $usr, $pwd);
mysql_select_db($db);

$sql = "SELECT DISTINCT name" .
       " FROM aos_products" .
       " order by name asc;";

$query = mysql_query($sql);      

while($row = mysql_fetch_row($query)) {
     echo "<option value='$row[0]'>$row[0]</option>";
}

mysql_close();

?>

