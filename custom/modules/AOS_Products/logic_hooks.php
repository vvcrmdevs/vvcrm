<?php
// Bill Forsyth
// Viking Vehicles project
//  04/04/2016
// Add related (dealership module) data to product (vehicle) 
// listview file 
$hook_version = 1; 
$hook_array = Array(); 
// position, file, function

$hook_array['process_record'] = Array();

$hook_array['process_record'][] = Array(1, 
    'Dealership Info', 
    'custom/modules/AOS_Products/dealership_info.php',
    'dealershipInfo', 
   'updateVehicle');


//$hook_array['after_retrieve'] = Array();

//$hook_array['after_retrieve'][] = Array(1, 
//    'Dealership Info', 
 //   'custom/modules/AOS_Products/dealership_info.php',
 //   'dealershipInfo', 
 //   'updateVehicle');

?>
