<?php
// @Author: Bill Forsyth
// @Package VVCRM
// Cascading dropdown for vehicle search assist func
// 
define('__ROOT__', dirname(dirname(dirname(dirname(__FILE__))))); 
require_once(__ROOT__.'/config.php'); 

$svr = $sugar_config[dbconfig]['db_host_name'];
$usr =  $sugar_config[dbconfig]['db_user_name'];
$pwd = $sugar_config[dbconfig]['db_password'];
$db = $sugar_config[dbconfig]['db_name'];

$brand = $_POST['brand'];
$model = $_POST['model'];

if (isset($brand) and isset($model)) {
    mysql_connect($svr, $usr, $pwd);
    mysql_select_db($db);
    $sql = "SELECT DISTINCT c.variant_c, c.variant_c  ".   
            " FROM aos_products_cstm c".
             " inner join aos_products p on p.id = c.id_c".               
             " where length(c.variant_c ) > 0".
              "     and lower(p.name) like '" . strtolower($brand) . "'" .
              "     and lower(p.part_number) like '" . strtolower($model) . "'" .
             " and deleted=0 order by c.variant_c asc;";    
     
   $query = mysql_query($sql);
   echo "<option value=''>All variants</option>";
    while($row = mysql_fetch_row($query)) {
        echo "<option value='$row[0]'>$row[1]</option>";
    }
    mysql_close();
}



?>


