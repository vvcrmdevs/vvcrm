<?php
/**
 * @package VVCRM
 * @author Bill forsyth
 * @date   11/05/2016
 * @description Automatically create quote for vehicles select in 
 *              AOS_Products (Vehicles) detail view. Function attached to onclick event
 *              to button in view/veiw.list.php
 * 
 * 
 */

class CustomAOS_ProductsController extends SugarController
{
    public function action_make_quote() {
           
        if ( !empty($_REQUEST['uid']) ) {
            $recordIds = explode(',',$_REQUEST['uid']); // product ids
            $q_id = create_guid();          // Quote UUID
            $g_id = create_guid();          // Quote Group UUID 
            //
            $bean = SugarModule::get($_REQUEST['module'])->loadBean();
            //
            // Create new quote record
            $quote = new AOS_Quotes();            
            $quote->id = $q_id; 
            $quote->new_with_id = TRUE;
            $quote->name = "Auto quote";
            $quote->modified_user_id = '1';
            $quote->created_by = '1';
            $quote->description = "";
            $quote->assigned_user_id = $bean->assigned_user_id;
            $quote->currency_id = $bean->currency_id;
            $quote->shipping_address_country = 'Australia';
            $quote->billing_address_country = 'Australia';
            //
            $expiredate = date('Y-m-d', strtotime('+7 days'));
            $convert_tz = FALSE;           
            $quote->expiration = $expiredate; //TimeDate::getInstance()->to_db_date($expiredate,$convert_tz );
            $quote->number = 1;            
            $quote->stage = 'Draft';
            $quote->invoice_status = 'Not Invoiced';
            //
            $quote->save();          
            //
            $group = new AOS_Line_Item_Groups();
            $group->id = $g_id;  
            $quote->new_with_id = TRUE;
            $group->modified_user_id = '1';
            $group->created_by = '1';
            $group->parent_id = $q_id;
            $group->parent_type='AOS_Quotes';
            $group->number = 1;
            $group->assigned_user_id = $bean->assigned_user_id;
            $group->currency_id = $bean->currency_id;
            $group->name ="Auto group" ; 
            //
            $group->save();
            //
            $j = 0;
            foreach ( $recordIds as $recordId ) {
                $bean = SugarModule::get($_REQUEST['module'])->loadBean();
                $bean->retrieve($recordId);
                //
                $line_item = new AOS_Products_Quotes();
                //
                $line_item->modified_user_id = '1';
                $line_item->created_by = '1';
                $line_item->parent_id = $q_id;
                $line_item->parent_type='AOS_Quotes';
                $line_item->product_id = $bean->id;
                $line_item->group_id = $g_id;  
                $line_item->assigned_user_id = $bean->assigned_user_id;
                $line_item->product_qty = 1;
                $line_item->number = ++$j;
                $line_item->currency_id = $bean->currency_id;
                $line_item->product_list_price=$bean->price;
                $line_item->vat = 10;
                
                //
                $line_item->name                    = $bean->name;   // brand
                $line_item->part_number             = $bean->part_number;  // model 
                $line_item->vik_dealership_id_c     = $bean->vik_dealership_id_c;
                $line_item->variant_c               = $bean->variant_c;
                $line_item->mats_c                  = $bean->mats_c;
                $line_item->paint_c                 = $bean->paint_c;
                $line_item->state_c                 = $bean->state_c;
                $line_item->hybrid_or_electric_c    = $bean->hybrid_or_electric_c;
                $line_item->engine_cylinders_c      = $bean->engine_cylinders_c;
                $line_item->fuel_economy_combined_c = $bean->fuel_economy_combined_c;
                $line_item->dealer_c                = $bean->dealer_c;
                //
                $line_item->towbar_c                = $bean->towbar_c;
                $line_item->tint_c                  = $bean->tint_c;
                $line_item->registration_and_ctp_c  = $bean->registration_and_ctp_c;
                $line_item->stamp_duty_c            = $bean->stamp_duty_c; 
                $line_item->luxury_car_tax_c        = $bean->luxury_car_tax_c; 
                $line_item->predelivery_c           = $bean->predelivery_c;
                $line_item->other_accessories_c     = $bean->other_accessories_c;
                $line_item->best_monthly_discount_c = $bean->best_monthly_discount_c;
                $line_item->national_fleet_discount_c= $bean->national_fleet_discount_c;
                $line_item->general_fleet_discount_c= $bean->general_fleet_discount_c;
                //
                //$key = 'product_';
                //$_POST[$key . 'id'][$j-1] = $line_item->id;
                $line_item->save(); 
            }
            // open the new auto quote for editing
            header('Location: index.php?module=AOS_Quotes&action=EditView&record='.$q_id);
 	}
    }
}
    
    
