<form id="ConfigureSettings" name="ConfigureSettings" enctype='multipart/form-data' method="POST"
      action="index.php?module=Administration&action=VikAdmin&do=save">

    <span class='error'>{$error.main}</span>

    <table width="100%" cellpadding="0" cellspacing="1" border="0" class="actionsContainer">
        <tr>
            <td>
                {$BUTTONS}
            </td>
        </tr>
    </table>

    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view">
        <tr><th align="left" scope="row" colspan="4"><h4>Luxury Car Tax</h4></th>
        <tr>
            <td  scope="row" width="200">Luxury Car Tax Threshold:</td>
            <td  >
                <input type='text' size='10' name='vikingvehicles_luxurythreshold' value='{$config.vikingvehicles.luxurythreshold}' > 
            </td>
        </tr>
    </table>


    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view">
        <tr><th align="left" scope="row" colspan="4"><h4>Green Luxury Tax Threshold</h4></th>
        </tr>
        <tr>
            <td  scope="row" width="200">Fuel Efficient Luxury Tax Threshold: </td>
            <td  >
                <input type='text' size='10' name='vikingvehicles_greenthreshold' value='{$config.vikingvehicles.greenthreshold}' >
            </td>
        </tr>
    </table>

    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view">
        <tr><th align="left" scope="row" colspan="4"><h4>Luxury Car Tax Rate</h4></th>
        </tr>
        <tr>
            <td  scope="row" width="200">Luxury Car Tax Rate: </td>
            <td  >
                <input type='text' size='10' name='vikingvehicles_lctrate' value='{$config.vikingvehicles.lctrate}' ><span> %</span>
            </td>
        </tr>
    </table>
            
            
    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view">
        <tr><th align="left" scope="row" colspan="4"><h4>Green Rating Fuel Efficency Threshold</h4></th>
        </tr>
        <tr>
            <td  scope="row" width="200">Litres Per 100 Kilometre: </td>
            <td  >
                <input type='text' size='10' name='vikingvehicles_litresper100km' value='{$config.vikingvehicles.litresper100km}' ><span> L/100Km</span>
            </td>
        </tr>
    </table>

    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view">
        <tr><th align="left" scope="row" colspan="4"><h4>Viking Vehicles Fees</h4></th>
        </tr>
        <tr>
            <td  scope="row" width="200">Viking Vehicles Charges:</td>
               <td  >
                    <input type='text' size='10' name='vikingvehicles_charges_vikingfee' value='{$config.vikingvehicles.charges.vikingfee}' >
               </td>
            
            <td  scope="row" width="200">Customer Charges: </td>
               <td  >
                    <input type='text' size='10' name='vikingvehicles_charges_clientfee' value='{$config.vikingvehicles.charges.clientfee}' >
               </td>           
        </tr>
    </table>

    <div style="padding-top: 2px;">
        {$BUTTONS}
    </div>
    {$JAVASCRIPT}
</form>
