<?php 
 //WARNING: The contents of this file are auto-generated



/* 
 * @package VVCRM
 * @Author Billl forsyth
 * 
 * @Date 16/05/2016
 * @description: global options for Viking Vehicles package that can be set from Admin page
 * 
 */

// initialize a temp array that will hold the options we want to create
$links = array();

// add button1 to $links
$links['vik_Admin']['link1'] = array(

    // pick an image from /themes/Sugar5/images
    // and drop the file extension
    'Administration',
    // title of the link 
    'LBL_VIK_ADMIN_LINK1_DESCRIPTION',
    // description for the link
    'LBL_VIK_ADMIN_LINK1_DESCRIPTION',
    // where to send the user when the link is clicked
    './index.php?module=Administration&action=VikAdmin',
);

// add link2 to $links
// this link uses labels instead of hard-coded strings
// since this is in the scope of the Administration module, 
// you need to add these labels to the 
// Administration lang files in 
// /custom/Extension/modules/Administration/Ext/Language/abc_Module.php
//$links['vik_Admin']['link2'] = array(

// pick an image from /themes/Sugar5/images
// and drop the file extension
//'Administration',

// defined in /custom/Extension/modules/Administration/Ext/Language/abc_Module.php
//'LBL_VIK_ADMIN_LINK2_TITLE',

// defined in /custom/Extension/modules/Administration/Ext/Language/abc_Module.php
//'LBL_VIK_ADMIN_LINK2_DESCRIPTION',

// where to send the user when the link is clicked
//'./index.php?module=vik_Admin&action=link2_action', 

//);

// add our new admin section to the main admin_group_header array
$admin_group_header []= array(

    // The title for the group of links
    'Viking Vehicles', 

    // leave empty, it's used for something in /include/utils/layout_utils.php 
    // in the get_module_title() function
    '', 

    // set to false, it's used for something in /include/utils/layout_utils.php 
    // in the get_module_title() function
    false, 

    // the array of links that you created above
    // to be placed in this section
    $links, 

    // a description for what this section is about
    'Set Luxury Car Thresholds for current financial year'

);

?>