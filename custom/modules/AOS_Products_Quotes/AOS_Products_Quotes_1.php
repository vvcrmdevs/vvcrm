<?php
/**
 * @package VVCRM
 * @Author bill Forsyth
 * @Date 18/05/2016
 * @Description Custom save_lines() func to save checkbox values
 */

require_once('modules/AOS_Products_Quotes/AOS_Products_Quotes_sugar.php');

class AOS_Products_Quotes extends AOS_Products_Quotes_sugar
{

    function AOS_Products_Quotes()
    {
        parent::AOS_Products_Quotes_sugar();
    }

    function save_lines($post_data, $parent, $groups = array(), $key = '')
    {
        $line_count = isset($post_data[$key . 'name']) ? count($post_data[$key . 'name']) : 0;
        $j = 0;
        for ($i = 0; $i < $line_count; ++$i) {

            if ($post_data[$key . 'deleted'][$i] == 1) {
                $this->mark_deleted($post_data[$key . 'id'][$i]);
            } else {
                $product_quote = new AOS_Products_Quotes();
                foreach ($this->field_defs as $field_def) {
                    $field_name = $field_def['name'];
                    if (isset($post_data[$key . $field_name][$i])) {
                        
                        if (substr( $field_name, 0, 6 ) == 'select') {
                            // Bill F: update db field for checkboxes as 'checked' 
                            // suitecrm will not post checkbox state if 'false'
                            // so they never get here. Only get here if set to true
                            $product_quote->$field_name = '1'; 
                        } else
                            $product_quote->$field_name = $post_data[$key . $field_name][$i];
                        
                    } else if (substr( $field_name, 0, 6 ) == 'select') {
                        // Bill F: update db field for update checkboxes as 'unchecked' 
                        $product_quote->$field_name = '0';                        
                    }
                }
                // Added by Bill Forsyth 09/06/2016
                $product_quote->vv_fees_c = $post_data['vv_fees'] ;
                $product_quote->vv_customer_fees_c = $post_data['vv_customer_fees'] ; 
                
                if (isset($post_data[$key . 'group_number'][$i])) {
                    $product_quote->group_id = $groups[$post_data[$key . 'group_number'][$i]];
                }
                
                if (trim($product_quote->product_id) != '' 
                        && trim($product_quote->name) != '' 
                        && trim($product_quote->product_list_price) != ''){
                    
                    /* Bill F: changed unit_ptice to list_price, cause unit_price was not working for some reason?? */
                    $product_quote->number = ++$j;
                    $product_quote->assigned_user_id = $parent->assigned_user_id;
                    $product_quote->parent_id = $parent->id;
                    $product_quote->currency_id = $parent->currency_id;
                    $product_quote->parent_type = $parent->object_name;
                    $product_quote->save();
                    
                    $_POST[$key . 'id'][$i] = $product_quote->id;
                }
            }
        }
    }

    function save($check_notify = FALSE)
    {
        require_once('modules/AOS_Products_Quotes/AOS_Utils.php');
        perform_aos_save($this);
        // Modded by Bill Forsyth
        //  added the 3 lines below. There was no code to save line-items in
        //  // line-items save....only in group save; which is fine, 
        //  // unless group option was turned off
       // $productQuote = new AOS_Products_Quotes();
       // $productQuote->save_lines($post_data, $parent, $groups, 'product_');
      //  $productQuote->save_lines($post_data, $parent, $groups, 'service_');
        //
        parent::save($check_notify);
    }

    /**
     * @param $parent SugarBean
     */
    function mark_lines_deleted($parent)
    {

        require_once('modules/Relationships/Relationship.php');
        $product_quotes = $parent->get_linked_beans('aos_products_quotes', $this->object_name);
        foreach ($product_quotes as $product_quote) {
            $product_quote->mark_deleted($product_quote->id);
        }
    }
}