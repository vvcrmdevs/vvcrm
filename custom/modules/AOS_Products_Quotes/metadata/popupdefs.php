<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Products_Quotes',
    'varName' => 'AOS_Products_Quotes',
    'orderBy' => 'aos_products_quotes.name',
    'whereClauses' => array (
  'name' => 'aos_products_quotes.name',
  'variant_c' => 'aos_products_quotes_cstm.variant_c',
  'part_number' => 'aos_products_quotes.part_number',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'variant_c',
  5 => 'part_number',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'text',
    'link' => true,
    'label' => 'LBL_NAME',
    'sortable' => false,
    'width' => '10%',
    'name' => 'name',
  ),
  'variant_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_VARIANT',
    'width' => '10%',
    'name' => 'variant_c',
  ),
  'part_number' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PART_NUMBER',
    'width' => '10%',
    'name' => 'part_number',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '5%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'PART_NUMBER' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PART_NUMBER',
    'width' => '5%',
    'name' => 'part_number',
  ),
  'VARIANT_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_VARIANT',
    'width' => '20%',
    'name' => 'variant_c',
  ),
  'PRODUCT_LIST_PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRODUCT_LIST_PRICE',
    'currency_format' => true,
    'width' => '5%',
    'default' => true,
    'name' => 'product_list_price',
  ),
  'MATS_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_MATS',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'mats_c',
  ),
  'TOWBAR_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_TOWBAR',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'towbar_c',
  ),
  'TINT_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_TINT',
    'currency_format' => true,
    'width' => '5%',
    'name' => 'tint_c',
  ),
  'PRODUCT_COST_PRICE' => 
  array (
    'width' => '5%',
    'label' => 'LBL_PRODUCT_COST_PRICE',
    'default' => true,
    'name' => 'product_cost_price',
  ),
  'VAT_AMT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_VAT_AMT',
    'currency_format' => true,
    'width' => '5%',
    'default' => true,
    'name' => 'vat_amt',
  ),
  'PRODUCT_TOTAL_PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRODUCT_TOTAL_PRICE',
    'currency_format' => true,
    'width' => '5%',
    'default' => true,
    'name' => 'product_total_price',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
