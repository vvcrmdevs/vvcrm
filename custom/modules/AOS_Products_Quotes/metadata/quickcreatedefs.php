<?php
$module_name = 'AOS_Products_Quotes';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'dealer_c',
            'studio' => 'visible',
            'label' => 'LBL_DEALER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'part_number',
            'label' => 'LBL_PART_NUMBER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'variant_c',
            'label' => 'LBL_VARIANT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'mats_c',
            'label' => 'LBL_MATS',
          ),
          1 => 
          array (
            'name' => 'tint_c',
            'label' => 'LBL_TINT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'towbar_c',
            'label' => 'LBL_TOWBAR',
          ),
          1 => 
          array (
            'name' => 'paint_c',
            'label' => 'LBL_PAINT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'product_list_price',
            'label' => 'LBL_PRODUCT_LIST_PRICE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'product_unit_price',
            'label' => 'LBL_PRODUCT_UNIT_PRICE',
          ),
          1 => 
          array (
            'name' => 'product_cost_price',
            'label' => 'LBL_PRODUCT_COST_PRICE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'vat_amt',
            'label' => 'LBL_VAT_AMT',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'product_discount',
            'label' => 'LBL_PRODUCT_DISCOUNT',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'product_total_price',
            'label' => 'LBL_PRODUCT_TOTAL_PRICE',
          ),
        ),
        10 => 
        array (
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'parent_name',
            'label' => 'LBL_FLEX_RELATE',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
      ),
    ),
  ),
);
?>
