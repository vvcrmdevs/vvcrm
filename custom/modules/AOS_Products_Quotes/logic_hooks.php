<?php
// Bill Forsyth
// Viking Vehicles project
//  04/04/2016
// Add related (dealership module) data to product (vehicle) 
// listview file 
$hook_version = 1; 
$hook_array = Array(); 
//
// position, file, function
//
//$hook_array['server_round_trip'] = Array();
//$hook_array['server_round_trip'][] = Array(
        //Processing index. For sorting the array.
 //       1,        
        //Label. A string value to identify the hook.
   //     'auto_lineitems example', 
        
        //The PHP file where your class is located.
    //    'custom/modules/AOS_Product_Quotes/lineitems_hooks.php', 
        
        //The class the method is in.
     //   'lineitems_hooks_class', 
        
        //The method to call.
     //   'server_round_trip_method' 
   // );

$hook_array['after_retrieve'] = Array();

$hook_array['after_retrieve'][] = Array(1, 
    // 
    'auto_lineitems Info',     
    //The PHP file where your class is located.
    'custom/modules/AOS_Products_Quotes/lineitems_hooks.php',     
    //The class the method is in.
    'lineitems_hooks_class',        
    //The method to call.
    'server_round_trip_method'     
);

?>
