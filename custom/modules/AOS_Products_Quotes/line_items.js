/**
 * 
 * @package VVCRM
 * Whitehack P/L http://www.whitehack.com.au , 
 * @author Bill Forsyth
 * @description Customized lineitems.js, modded for Viking Vehicles 
 */

 var lineno;
 var prodln = 0;
 var servln = 0;
 var groupn = 0;
 var group_ids = {};
 //
 var disclaimer_part1 = 
"Please note: the discounting level you have chosen for this customer is "; 
//
 var disclaimer_part2 = 
"Please ensure that your customer meets the minimum requirements for this level" +
" of discount. Neither Viking Vehicles or the supplying dealership can be held " +
"to a price that has been incorrectly offered to a customer."; 


function setTableColSpan(){
  var tbl=document.getElementById('LBL_LINE_ITEMS');
  for (var j = 0, col; col = tbl.cells[j]; j++) {
          if (col.colspan === "3")
              col.colspan = "10";
  } 
}

 /**
 * Load Line Items
 */
function insertLineItems(product,group) {
    var type = 'product_';
    var ln = 0;
    var current_group = 'lineItems';
    var gid = product.group_id;

    if(typeof group_ids[gid] === 'undefined'){
	current_group = insertGroup();
	group_ids[gid] = current_group;
	for(var g in group){
		if(document.getElementById('group'+current_group + g) !== null){
			document.getElementById('group'+current_group + g).value = group[g];
		}             
	}
    } else {
	current_group = group_ids[gid];
    }

    if(product.product_id != '0' && product.product_id !== ''){
	ln = insertProductLine('product_group'+current_group,current_group);
	type = 'product_';
    } else {
	ln = insertServiceLine('service_group'+current_group,current_group);
	type = 'service_';
    }

    for(var p in product){
	if(document.getElementById(type + p + ln) !== null){
           // if (p.startsWith('dealer',0) ) 
             //       alert(product[p]);
            
            if (p.startsWith('select',0)) {
                // Bill F: need this to correctly set checkboxes  
                cb = document.getElementById(type + p + ln);                
                cb.checked = (product[p] === 1) ? true: false; 
            } else if(product[p] !== '' && isNumeric(product[p]) &&  p != 'vat'  && p != 'product_id' && p != 'name' &&  p != "part_number" ) {
                document.getElementById(type + p + ln).value = format2Number(product[p]);
            } else {
                document.getElementById(type + p + ln).value = product[p];
            }            
        } 
    }
    // Bill F. mod
    if (type === 'product_') {
        toggle_mats(ln); 
        toggle_tint(ln); 
        toggle_towbar(ln); 
        toggle_paint(ln);
    } else 
        calculateLine(ln,type);
}
/////////////////////////////////////////
/**
 * @function
 * @package VVCRM
 * @description Insert Mats price if selected
 * @author: bill forsyth
 * @date: 05/05/2016
 */
 function toggle_mats(ln) { 
    var htbx=document.getElementById('product_mats_c' + ln);  // hidden stored mats val
    var cb=document.getElementById('product_select_mats_c' + ln);  // user selection
    var tbx=document.getElementById('product_mats_c_amount' + ln); // visible display
       
     
    tbx.disabled = (parseFloat(htbx.value) === 'NaN' || parseFloat(htbx.value) == 0) ? true : false;
    cb.disabled =tbx.disabled;
    if (! cb.disabled ) { 
        if(cb.checked) {
            //var qty = fixnum(document.getElementById('product_product_qty' + ln).value);
             var v = unformat2Number(htbx.value);
            tbx.value = format2Number( v );     
        } else {  
            tbx.value='0.00' ;         
        }
    } else {
        cb.checked =false;
        tbx.value='N/A';
    }
    calculateLine(ln ,"product_");
 }
 
 /**
  * @function
  * @package VVCRM
  * @description Insert Tint price if selected
  * @author: bill forsyth
  * @date: 06/05/2016
  */
 function toggle_tint(ln) {
    var cb=document.getElementById('product_select_tint_c' + ln);
    var tbx=document.getElementById('product_tint_c_amount' + ln);
    var htbx=document.getElementById('product_tint_c' + ln);
    
    
   tbx.disabled = (parseFloat(htbx.value) === 'NaN' || parseFloat(htbx.value) == 0) ? true : false;
    cb.disabled =tbx.disabled;
    if (!cb.disabled) { 
        if(cb.checked) {
            //var qty = fixnum(document.getElementById('product_product_qty' + ln).value);
            var v = unformat2Number(htbx.value);
            tbx.value = format2Number( v);   
         } else { 
             tbx.value='0.00' ;         
        }
    } else {
        cb.checked =false;
        tbx.value='N/A';
    }
    calculateLine(ln ,"product_");
 }
 
 /**
  * @function
  * @package VVCRM
  * @description Insert Towbar price if selected
  * @author: bill forsyth
  * @date: 06/05/2016
  */
 function toggle_towbar(ln) { 
    var cb=document.getElementById('product_select_towbar_c' + ln);
    var tbx=document.getElementById('product_towbar_c_amount' + ln);
    var htbx=document.getElementById('product_towbar_c' + ln);
        
    tbx.disabled = (parseFloat(htbx.value) === 'NaN' || parseFloat(htbx.value) == 0) ? true : false;
    cb.disabled =tbx.disabled;
    if (!cb.disabled) { 
        if(cb.checked) {
            var qty = fixnum(document.getElementById('product_product_qty' + ln).value);
            var v = unformat2Number(htbx.value);
            tbx.value = format2Number(qty * v);   
        } else { 
             tbx.value='0.00' ;         
        }
    } else {
        cb.checked =false;
         tbx.value='N/A';
    }
    calculateLine(ln ,"product_"); 
 }
 
 /**
  * @function
  * @package VVCRM
  * @description  Insert Paint price if selected
  * @author: bill forsyth
  * @date: 06/05/2016
  */
 function toggle_paint(ln) {     
    var cb=document.getElementById('product_select_paint_c' + ln);
    var tbx=document.getElementById('product_paint_c_amount' + ln);
    var htbx=document.getElementById('product_paint_c' + ln);
   
    
    tbx.disabled = (parseFloat(htbx.value) === 'NaN' || parseFloat(htbx.value) == 0) ? true : false;
    cb.disabled =tbx.disabled;
    if (!cb.disabled) { 
        if(cb.checked) {
            var qty = fixnum(document.getElementById('product_product_qty' + ln).value);
            var v = unformat2Number(htbx.value);
            tbx.value = format2Number(qty * v);              
        } else { 
            tbx.value='0.00' ;         
        }
        
     } else {
        cb.checked =false;
        tbx.value='N/A';
    }
    calculateLine(ln ,"product_");
 }

/////////////////////////////////////
function insertProductLine(tableid, groupid) {
    if(!enable_groups){
        tableid = "product_group0";
    }
    //
    if (document.getElementById(tableid + '_head') !== null) {
        document.getElementById(tableid + '_head').style.display = "";
    }
    //
    var vat_hidden = document.getElementById("vathidden").value;
    var discount_hidden = document.getElementById("discounthidden").value;
    //
    sqs_objects["product_name[" + prodln + "]"] = {
        "form": "EditView",
        "method": "query",
        "modules": ["AOS_Products"],
        "group": "or",        
        "field_list": ["name", 
            "id",
            "part_number",
            "variant_c",
            "registration_and_ctp_c",
            "predelivery_c",
            "state_c",
            "dealer_c",
            "stamp_duty_c",
            "luxury_car_tax_c",  
            "mats_c",
            "towbar_c",
            "tint_c",
            "paint_c",
            "other_accessories_c",           
            "best_monthly_discount_c",
            "national_fleet_discount_c",
            "general_fleet_discount_c",
            "fuel_economy_combined_c",
            "engine_cylinders_c",
            "hybrid_or_electric_c",
            "price",
            "description",
            "currency_id"],
        "populate_list": ["product_name[" + prodln + "]",
            "product_product_id[" + prodln + "]",             
            "product_part_number[" + prodln + "]", 
            "product_variant_c[" + prodln + "]",
            "product_registration_and_ctp_c[" + prodln + "]",
            "product_predelivery_c[" + prodln + "]", 
            "product_state_c[" + prodln + "]", 
            "product_dealer_c[" + prodln + "]", 
            "product_stamp_duty_c[" + prodln + "]", 
            "product_luxury_car_tax_c[" + prodln + "]",
            "product_mats_c[" + prodln + "]",
            "product_towbar_c[" + prodln + "]",
            "product_tint_c[" + prodln + "]",
            "product_paint_c[" + prodln + "]",
            "product_other_accessories_c[" + prodln + "]",  
            "product_best_monthly_discount_c[" + prodln + "]",
            "product_national_fleet_discount_c[" + prodln + "]",
            "product_general_fleet_discount_c[" + prodln + "]",
            "product_fuel_economy_combined_c[" + prodln + "]",
            "product_engine_cylinders_c[" + prodln + "]",
            "product_hybrid_or_electric_c[" + prodln + "]",
            "product_product_list_price[" + prodln + "]", 
            "product_item_description[" + prodln + "]", 
            "product_currency[" + prodln + "]"],
        "required_list": ["product_id[" + prodln + "]"],
        "conditions": [{
            "name": "name",
            "op": "like_custom",
            "end": "%",
            "value": ""
        }],
        "order": "name",
        "limit": "30",
        "post_onblur_function": "formatListPrice(" + prodln + ");",
        "no_match_text": "No Match"
    };
    //  
    sqs_objects["product_part_number[" + prodln + "]"] = {
        "form": "EditView",
        "method": "query",
        "modules": ["AOS_Products"],
        "group": "or",
        "field_list": ["part_number", 
            "name", 
            "id",
            "variant_c",
            "registration_and_ctp_c",             
            "predelivery_c", 
            "state_c",
            "dealer_c",
            "stamp_duty_c",
            "luxury_car_tax_c",
            "mats",
            "towbar_c",
            "tint_c",
            "paint_c",
            "other_accessories_c",
            "best_monthly_discount_c",
            "national_fleet_discount_c",
            "general_fleet_discount_c",
            "fuel_economy_combined_c",
            "engine_cylinders_c",
            "hybrid_or_electric_c",
            "price",
            "description",
            "currency_id"],
        "populate_list": ["product_part_number[" + prodln + "]", 
            "product_name[" + prodln + "]", 
            "product_product_id[" + prodln + "]", 
            "product_variant_c[" + prodln + "]",
            "product_registration_and_ctp_c[" + prodln + "]",
            "product_predelivery_c[" + prodln + "]",
            "product_state_c[" + prodln + "]", 
            "product_dealer_c[" + prodln + "]", 
            "product_stamp_duty_c[" + prodln + "]", 
            "product_luxury_car_tax_c[" + prodln + "]",
            "product_mats_c[" + prodln + "]",
            "product_towbar_c[" + prodln + "]",
            "product_tint_c[" + prodln + "]",
            "product_paint_c[" + prodln + "]",
            "product_other_accessories_c[" + prodln + "]",
            "product_best_monthly_discount_c[" + prodln + "]",
            "product_national_fleet_discount_c[" + prodln + "]", 
            "product_general_fleet_discount_c[" + prodln + "]",
            "product_fuel_economy_combined_c[" + prodln + "]",
            "product_engine_cylinders_c[" + prodln + "]",
            "product_hybrid_or_electric_c[" + prodln + "]",
            "product_product_list_price_c[" + prodln + "]", 
            "product_item_description[" + prodln + "]", 
            "product_currency[" + prodln + "]"],
        "required_list": ["product_id[" + prodln + "]"],
        "conditions": [{
            "name": "part_number",
            "op": "like_custom",
            "end": "%",
            "value": ""
        }],
        "order": "name",
        "limit": "30",
        "post_onblur_function": "formatListPrice(" + prodln + ");",
        "no_match_text": "No Match"
    };    
    ///////////////////////////////////////////////
    tablebody = document.createElement("tbody");
    tablebody.id = "product_body" + prodln;
    
    document.getElementById(tableid).appendChild(tablebody);

    var x = tablebody.insertRow(-1);
    x.id = 'product_line' + prodln;
    ////////////////////////////////////////////
    //
     
    var a0 = x.insertCell(0);
    a0.innerHTML = "<textarea tabindex='116' name='product_dealer_c[" + prodln + "]' id='product_dealer_c" + 
                  prodln + "' rows='1' cols='20' autocomplete='off' readonly='readonly'></textarea>";
        
    var b = x.insertCell(1);
    b.innerHTML = "<input style='width:80px;' class='sqsEnabled' autocomplete='off' type='text' name='product_name[" +
            prodln + "]' id='product_name" + 
            prodln + "' maxlength='50' value='' title='' tabindex='116' value=''>";
    //
    // Hidden fields
    b.innerHTML += "<input type='hidden' name='product_product_id[" + prodln + "]' id='product_product_id" + prodln + "' value=''/>";
    b.innerHTML += "<input type='hidden' name='product_best_monthly_discount_c[" + prodln + "]' id='product_best_monthly_discount_c" + prodln + "' value='' />";
    b.innerHTML += "<input type='hidden' name='product_national_fleet_discount_c[" + prodln + "]' id='product_national_fleet_discount_c" + prodln + "' value='' />";
    b.innerHTML += "<input type='hidden' name='product_general_fleet_discount_c[" + prodln + "]' id='product_general_fleet_discount_c" + prodln + "' value='' />";
    b.innerHTML += "<input type='hidden' name='product_fuel_economy_combined_c[" + prodln + "]' id='product_fuel_economy_combined_c" + prodln + "' value='' />";
    b.innerHTML += "<input type='hidden' name='product_engine_cylinders_c[" + prodln + "]' id='product_engine_cylinders_c" + prodln + "' value='' />";
    b.innerHTML += "<input type='hidden' name='product_hybrid_or_electric_c[" + prodln + "]' id='product_hybrid_or_electric_c" + prodln + "' value='' />";
    b.innerHTML += "<input type='hidden' name='product_state_c[" + prodln + "]' id='product_state_c" + prodln + "' value='' />";
    b.innerHTML += "<input type='hidden' name='product_predelivery_c[" + prodln + "]' id='product_predelivery_c" + prodln + "' value='' />";

    //
    //
    //
    var b1 = x.insertCell(2);
    b1.innerHTML = "<input style='width:64px;' class='sqsEnabled' autocomplete='off' type='text' name='product_part_number[" + 
            prodln + "]' id='product_part_number" + 
            prodln + "' maxlength='50' value='' title='' tabindex='116' value=''>";
    //
    // modded Bill Forsyth  02/05/2016 Added variant text field to line items
    // Variant
    // var b2 = x.insertCell(3);
    // b2.innerHTML = "<input style='width:300px;'  type='text' name='product_variant_c[" + 
      //      prodln + "]' id='product_variant_c" + 
      //      prodln + "' maxlength='80' value='' title='' tabindex='116'>";
    
    var b3 = x.insertCell(3);
    b3.innerHTML = "<button title='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_TITLE') + "' accessKey='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_KEY') + "' type='button' tabindex='116' class='button' value='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_LABEL') + "' name='btn1' onclick='openProductPopup(" + prodln + ");'><img src='themes/default/images/id-ff-select.png' alt='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_LABEL') + "'></button>";
    //
    ////////////////////////////////////////////////////////////////////////////////
    // List Price
    var c = x.insertCell(4);
    c.innerHTML = "<input style='text-align: right; width:104px;' type='text' name='product_product_list_price[" + 
            prodln + "]' id='product_product_list_price" + 
            prodln + "' size='11' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" +
            prodln + ",\"product_\");'><input type='hidden' name='product_product_cost_price[" + 
            prodln + "]' id='product_product_cost_price" + 
            prodln + "' value=''  />";
    //
    // pre-delivery and fees 
    var c1 = x.insertCell(5);
    c1.innerHTML = "<input type='text' style='text-align: right; width:68px;'\n\ name='product_predelivery_and_fees[" + 
            prodln + "]' id='product_predelivery_and_fees" + 
            prodln + "' size='11' maxlength='50' value='' title='' tabindex='116' readonly='readonly' onblur='calculateLine(" + 
            prodln + ",\"product_\");' onblur='calculateLine(" + 
            prodln + ",\"product_\");'/>";
    //
    //
    // modded Bill Forsyth 
    // Rego + CTP
    var c2 = x.insertCell(6);
    c2.innerHTML = "<input style='text-align: right; width:68px;' type='text' name='product_registration_and_ctp_c[" + 
            prodln + "]' id='product_registration_and_ctp_c" +
            prodln + "' size='11' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" + 
            prodln + ",\"product_\");'><input type='hidden' name='product_registration_and_ctp_c_amount[" + 
            prodln + "]' id='product_registration_and_ctp_c_amount" +
            prodln + "' value=''  />";
    
    var d = x.insertCell(7);
    d.innerHTML = "<input type='text' style='text-align: right; width:80px;' name='product_product_discount[" + 
            prodln + "]' id='product_product_discount" + 
            prodln + "' size='12' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" + 
            prodln + ",\"product_\");' onblur='calculateLine(" + 
            prodln + ",\"product_\");'><input type='hidden' name='product_product_discount_amount[" + 
            prodln + "]' id='product_product_discount_amount" + 
            prodln + "' value=''  />";
    d.innerHTML += "<select tabindex='116' name='product_discount[" + 
            prodln + "]' id='product_discount" + 
            prodln + "' onchange='calculateLine(" + 
            prodln + ",\"product_\");'>" + discount_hidden + "</select>";
    //
    //////////////////////////////////////////////////////////////////////////////////////
    // Optional Accessories
    // 
    // mats accessory
    var d1 = x.insertCell(8);
    d1.innerHTML = "<input type='checkbox'" + 
            " id='product_select_mats_c" + prodln + "' name ='product_select_mats_c[" + prodln + "]'" + 
            " value=''  onClick='toggle_mats(" + prodln + ")'>";
    //           
    d1.innerHTML += "<input type='text' style='text-align: right; width:42px;'"+
            " name='product_mats_c_amount[" +  prodln + "]' id='product_mats_c_amount" +  prodln + "'"+
            " size='11' maxlength='16' value='0.00' title='' tabindex='116'"+
            " readonly='readonly'" + 
            " onblur='calculateLine(" +  prodln + ",\"product_\");'>"; 
     
    d1.innerHTML +="<input type='hidden'" +
            " name='product_mats_c[" + prodln + "]' id='product_mats_c" +  prodln + "'" + 
            " onload='toggle_mats(" +  prodln + ");' " + 
            " value=''  />";
 
    // tint accessory
    var d2 = x.insertCell(9);
    d2.innerHTML = "<input type='checkbox'" + 
            " id='product_select_tint_c" + prodln + "' name ='product_select_tint_c[" + prodln + "]'" + 
            " value=''  onClick='toggle_tint(" + prodln + ")'>";
    //          
    d2.innerHTML += "<input type='text' style='text-align: right; width:42px;'" + 
            " name='product_tint_c_amount[" +  prodln + "]' id='product_tint_c_amount" +   prodln + "'" + 
            " size='11' maxlength='16' value='0.00' title='' tabindex='116' readonly='readonly'" +
             " onblur='calculateLine(" +  prodln + ",\"product_\");' >";
     
    d2.innerHTML +="<input type='hidden' name='product_tint_c[" + 
            prodln + "]' id='product_tint_c" +
            prodln + "' value=''  />";
     
    // paint accessory
    var d3 = x.insertCell(10);
    d3.innerHTML = "<input type='checkbox'" + 
            " id='product_select_paint_c" + prodln + "'" + 
            " name ='product_select_paint_c[" + prodln + "]'" + 
            " value=''  onClick='toggle_paint(" + prodln + ")'>";
    //          
    d3.innerHTML += "<input type='text' style='text-align: right; width:42px;' "+
            "name='product_paint_c_amount[" + prodln + "]'" + 
            " id='product_paint_c_amount" + prodln + "'" + 
            " size='11' maxlength='16' value='0.00' title='' tabindex='116' readonly='readonly'"+
            " onblur='calculateLine(" +  prodln + ",\"product_\");' >"; 
    
    d3.innerHTML +="<input type='hidden' name='product_paint_c[" + prodln + "]'" +
            " id='product_paint_c" + prodln + "'" + 
            " value=''  />";
    
    // towbar accessory
    var d4 = x.insertCell(11);
    d4.innerHTML = "<input type='checkbox'" + 
            " id='product_select_towbar_c" + prodln + "' name ='product_select_towbar_c[" + prodln + "]'" + 
            " value=''  onClick='toggle_towbar(" + prodln + ")'>";
    //           
    d4.innerHTML += "<input type='text' style='text-align: right; width:42px;'" + 
            " name='product_towbar_c_amount[" + prodln + "]' id='product_towbar_c_amount" +  prodln + "'" + 
            " size='11' maxlength='16' value='0.00' title='' tabindex='116' readonly='readonly'" + 
            " onblur='calculateLine(" + prodln + ",\"product_\");' >"; 
    //
    d4.innerHTML +="<input type='hidden'" + 
            " name='product_towbar_c[" + prodln + "]' id='product_towbar_c" + prodln + "'" +
            " value=''  />";
    
    //
    // other accessory
    var d5 = x.insertCell(12);              
    d5.innerHTML += "<input type='text' style='text-align: right; width:68px;' name='product_other_accessories_c[" + 
            prodln + "]' id='product_other_accessories_c" + 
            prodln + "' size='11' maxlength='16' value='0.00' title='' tabindex='116' onblur='calculateLine(" + 
            prodln + ",\"product_\");' >";    
   
    /////////////////////////////////////////////////////////////////////////////////
    // modded Bill Forsyth 
    // GST
    var e = x.insertCell(13);
    e.innerHTML = "<input type='text' style='text-align: right; width:48px;' name='product_vat_amt[" + 
            prodln + "]' id='product_vat_amt" + 
            prodln + "' size='11' maxlength='250' value='' title='' tabindex='116' readonly='readonly'>";
    e.innerHTML += "<select tabindex='116' name='product_vat[" + 
            prodln + "]' id='product_vat" + 
            prodln + "' onchange='calculateLine(" + 
            prodln + ",\"product_\");'>" + vat_hidden + "</select>";
    //
    //
    // modded Bill Forsyth 
    // LCT
    var h = x.insertCell(14);
    h.innerHTML = "<input style='text-align: right; width:48px;' type='text' name='product_luxury_car_tax_c[" + 
            prodln + "]' id='product_luxury_car_tax_c" +
            prodln + "' size='11' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" + 
            prodln + ",\"product_\");'><input type='hidden' name='product_luxury_car_tax_c_amount[" + 
            prodln + "]' id='product_luxury_car_tax_c_amount" +
            prodln + "' value=''  />";
    //
    // modded Bill Forsyth 
    // Stamp Duty
    var i = x.insertCell(15);
    i.innerHTML = "<input style='text-align: right; width:60px;' type='text'" +
            " name='product_stamp_duty_c[" +  prodln + "]' id='product_stamp_duty_c" +  prodln + "'" +
            " size='11' maxlength='50' value='' title='' readonly='readonly' tabindex='116' >";
    //
    // Drive Away
    var j = x.insertCell(16);
    j.innerHTML = "<input type='text' style='text-align: right; width:88px;' name='product_product_total_price[" + 
            prodln + "]' id='product_product_total_price" + 
            prodln + "' size='11' maxlength='50' value='' title='' tabindex='116' readonly='readonly'><input type='hidden' name='product_group_number[" + 
            prodln + "]' id='product_group_number" + 
            prodln + "' value='"+groupid+"'>";
    //
    //
    var k = x.insertCell(17);
    k.innerHTML = "<input type='hidden' name='product_currency[" + 
            prodln + "]' id='product_currency" + 
            prodln + "' value=''><input type='hidden' name='product_deleted[" + 
            prodln + "]' id='product_deleted" + 
            prodln + "' value='0'><input type='hidden' name='product_id[" + 
            prodln + "]' id='product_id" + 
            prodln + "' value=''><button type='button' id='product_delete_line" + 
            prodln + "' class='button' value='" + 
            SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_PRODUCT_LINE') + 
            "' tabindex='116' onclick='markLineDeleted(" + 
            prodln + ",\"product_\")'><img src='themes/default/images/id-ff-clear.png' alt='" + 
            SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_PRODUCT_LINE') +
            "'></button><br>";
    //
    //
     
    var a = x.insertCell(18);
    
    a.innerHTML = "<input type='hidden' name='product_product_qty[" + prodln + "]'" + " id='product_product_qty" + prodln +
            "' size='5' value='' title='' tabindex='116' onblur='Quantity_format2Number(" + 
            prodln + ");calculateLine(" + prodln + ",\"product_\");'>";
  
    if (typeof currencyFields !== 'undefined'){
        currencyFields.push("product_registration_and_ctp_c" + prodln);
        currencyFields.push("product_product_list_price" + prodln);
        currencyFields.push("product_product_cost_price" + prodln);   
        currencyFields.push("product_predelivery_c" + prodln);    
        currencyFields.push("product_vat_amt" + prodln);
        currencyFields.push("product_product_total_price" + prodln);
        currencyFields.push("product_luxury_car_tax_c" + prodln);
        currencyFields.push("product_towbar_c" + prodln);
        currencyFields.push("product_tint_c" + prodln);
        currencyFields.push("product_paint_c" + prodln);
        currencyFields.push("product_towbar_c" + prodln);
        currencyFields.push("product_other_accessories_c" + prodln);  
        currencyFields.push("product_best_monthly_discount_c" + prodln); 
        currencyFields.push("product_national_fleet_discount_c" + prodln); 
        currencyFields.push("product_general_fleet_discount_c" + prodln);        
    }
    //
    enableQS(true);
    //QSFieldsArray["EditView_product_name"+prodln].forceSelection = true;
    //
    var y = tablebody.insertRow(-1);
    y.id = 'product_note_line' + prodln;
    //
    y.insertCell(0);
    // modded Bill Forsyth  02/05/2016 Added variant text field to line items
    // Variant   
    //
    var l = y.insertCell(1);
    l.colSpan = "5";
    l.innerHTML = "<span style='vertical-align: top;'>" + "Model Variant" +
            // SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_VARIANT') + 
            " :&nbsp;&nbsp;</span>";            
    l.innerHTML += "<textarea tabindex='116' name='product_variant_c[" + 
            prodln + "]' id='product_variant_c" + 
            prodln + "' rows='3' cols='42'></textarea>&nbsp;&nbsp;";
    //
    var m = y.insertCell(2);
    m.colSpan = "7";
    m.style.color = "rgb(68,68,68)";
    m.innerHTML = "<span style='vertical-align: top;'>" + "Disclaimer" +
            //SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_DESCRIPTION') + 
            " :&nbsp;&nbsp;</span>";
    m.innerHTML += "<textarea tabindex='116' name='product_item_description[" + 
            prodln + "]' id='product_item_description" + 
            prodln + "' rows='3' cols='80'></textarea>&nbsp;&nbsp;";
    //
    var n = y.insertCell(3);
    n.colSpan = "4";
    n.style.color = "rgb(68,68,68)";
    n.innerHTML = "<span style='vertical-align: top;'>"  + 
            SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_NOTE') + 
            " :&nbsp;</span>";
    n.innerHTML += "<textarea tabindex='116' name='product_description[" + 
            prodln + "]' id='product_description" + 
            prodln + "' rows='3' cols='32'></textarea>&nbsp;&nbsp;";
    //
    //
    addToValidate('EditView','product_product_id'+prodln,'id',true,"Please choose a vehicle");
    //
    prodln++;
    //
    return prodln - 1;
}

/**
 * Open product popup
 */
function openProductPopup(ln){

	lineno=ln;
	var popupRequestData = {
		"call_back_function" : "setProductReturn",
		"form_name" : "EditView",
		"field_to_name_array" : {
			"id" : "product_product_id" + ln,
			"name" : "product_name" + ln,
                        "description" : "product_item_description" + ln,
                        "part_number" : "product_part_number" + ln,
                        //
                        // mods by Bill F 02/05/2016
                        "variant_c": "product_variant_c" + ln,
                        "registration_and_ctp_c": "product_registration_and_ctp_c" + ln,
                        "stamp_duty_c": "product_stamp_duty_c" + ln , 
                        "luxury_car_tax_c": "product_luxury_car_tax_c" + ln ,
			"predelivery_c" : "product_predelivery_c" + ln,
                        "state_c" : "product_state_c" + ln,
                        "dealer_c": "product_dealer_c" + ln,
                        "vik_dealership_id_c": "product_vik_dealership_id_c" +ln, 
                        "mats_c": "product_mats_c" + ln,
                        "towbar_c": "product_towbar_c" + ln ,
                        "tint_c": "product_tint_c" + ln ,
                        "paint_c": "product_paint_c" +ln ,
                        "other_accessories_c": "product_other_accessories_c" +ln ,
                        "best_monthly_discount_c": "product_best_monthly_discount_c" + ln, 
                        "national_fleet_discount_c": "product_national_fleet_discount_c" + ln, 
                        "general_fleet_discount_c": "product_general_fleet_discount_c" + ln, 
                        "fuel_economy_combined_c": "product_fuel_economy_combined_c" + ln,
                        "engine_cylinders_c": "product_engine_cylinders_c"+ ln,
                        "hybrid_or_electric_c": "product_hybrid_or_electric_c" + ln,
                        "select_mats_c": "product_select_mats_c" + ln ,
                        "select_towbar_c": "product_select_towbar_c" + ln ,
                        "select_paint_c": "product_select_paint_c" +ln ,
                        "select_tint_c": "product_select_tint_c" + ln ,
                        //
			"price" : "product_product_list_price" + ln,
                        "currency_id" : "product_currency" + ln
		}
	};

	open_popup('AOS_Products', 800, 850, '', true, true, popupRequestData);

}

function setProductReturn(popupReplyData){
	set_return(popupReplyData);
	formatListPrice(lineno);
}

function formatListPrice(ln){
    // Modded by Bill forsyth 03/05/2016
    //
    if (typeof currencyFields !== 'undefined'){
        var product_currency_id = document.getElementById('product_currency' + ln).value;
        product_currency_id = product_currency_id ? product_currency_id : -99; //Assume base currency if no id
        
        var product_currency_rate = get_rate(product_currency_id);
        
        var dollar_product_price = ConvertToDollar(document.getElementById('product_product_list_price' + ln).value, product_currency_rate);
        document.getElementById('product_product_list_price' + ln).value = format2Number(ConvertFromDollar(dollar_product_price, lastRate));
        
        var predelivery_and_fees = ConvertToDollar(document.getElementById('product_predelivery_and_fees' + ln).value, product_currency_rate);
        document.getElementById('product_predelivery_and_fees' + ln).value = format2Number(ConvertFromDollar(predelivery_and_fees, lastRate));
    
        var rego_and_CTP = ConvertToDollar(document.getElementById('product_registration_and_ctp_c' + ln).value, product_currency_rate);
        document.getElementById('product_registration_and_ctp_c' + ln).value = format2Number(ConvertFromDollar(rego_and_CTP, lastRate));

        var stampduty = ConvertToDollar(document.getElementById('product_stamp_duty_c' + ln).value, product_currency_rate);
        document.getElementById('product_stamp_duty_c' + ln).value = format2Number(ConvertFromDollar(stampduty, lastRate));

        var lct = ConvertToDollar(document.getElementById('product_luxury_car_tax_c' + ln).value, product_currency_rate);
        document.getElementById('product_luxury_car_tax_c' + ln).value = format2Number(ConvertFromDollar(lct, lastRate));
    
     //   var mats = ConvertToDollar(document.getElementById('product_mats_c' + ln).value, product_currency_rate);
     //  document.getElementById('product_mats_c' + ln).value = format2Number(ConvertFromDollar(mats, lastRate));

     //   var tint = ConvertToDollar(document.getElementById('product_tint_c' + ln).value, product_currency_rate);
     //   document.getElementById('product_tint_c' + ln).value = format2Number(ConvertFromDollar(tint, lastRate));
        
        var other = ConvertToDollar(document.getElementById('product_other_accessories_c' + ln).value, product_currency_rate);
        document.getElementById('product_other_accessories_c' + ln).value = format2Number(ConvertFromDollar(other, lastRate));
        
     //   var tb = ConvertToDollar(document.getElementById('product_towbar_c' + ln).value, product_currency_rate);
     //   document.getElementById('product_towbar_c' + ln).value = format2Number(ConvertFromDollar(tb, lastRate));
        
      //  var paint = ConvertToDollar(document.getElementById('product_paint_c' + ln).value, product_currency_rate);
     //   document.getElementById('product_paint_c' + ln).value = format2Number(ConvertFromDollar(paint, lastRate));
    }
    else
    {
        document.getElementById('product_product_list_price' + ln).value = format2Number(document.getElementById('product_product_list_price' + ln).value);
        document.getElementById('product_predelivery_c' + ln).value = format2Number(document.getElementById('product_predelivery_c' + ln).value);
        document.getElementById('product_registration_and_ctp_c' + ln).value = format2Number(document.getElementById('product_registration_and_ctp_c' + ln).value);
        document.getElementById('product_stamp_duty_c' + ln).value = format2Number(document.getElementById('product_stamp_duty_c' + ln).value);
        document.getElementById('product_luxury_car_tax_c' + ln).value = format2Number(document.getElementById('product_luxury_car_tax_c' + ln).value);
      //  document.getElementById('product_mats_c' + ln).value = format2Number(document.getElementById('product_mats_c' + ln).value);
      //  document.getElementById('product_tint_c' + ln).value = format2Number(document.getElementById('product_tint_c' + ln).value);
      //  document.getElementById('product_towbar_c' + ln).value = format2Number(document.getElementById('product_towbar_c' + ln).value);
      //  document.getElementById('product_paint_c' + ln).value = format2Number(document.getElementById('product_paint_c' + ln).value);
        document.getElementById('product_other_accessories_c' + ln).value = format2Number(document.getElementById('product_other_accessories_c' + ln).value);

    }
    calculateLine(ln,"product_");
}

/**
 * Insert Service Line
 */
function insertServiceLine(tableid, groupid) {

    if(!enable_groups){
        tableid = "service_group0";
    }
    if (document.getElementById(tableid + '_head') !== null) {
        document.getElementById(tableid + '_head').style.display = "";
    }

    var vat_hidden = document.getElementById("vathidden").value;
    var discount_hidden = document.getElementById("discounthidden").value;

    tablebody = document.createElement("tbody");
    tablebody.id = "service_body" + servln;
    document.getElementById(tableid).appendChild(tablebody);

    var x = tablebody.insertRow(-1);
    x.id = 'service_line' + servln;

    var a = x.insertCell(0);
    a.colSpan = "4";
    a.innerHTML = "<textarea name='service_name[" + servln + "]' id='service_name" + servln + "' size='16' cols='64' title='' tabindex='116'></textarea><input type='hidden' name='service_product_id[" + servln + "]' id='service_product_id" + servln + "' size='20' maxlength='50' value='0'>";

    var a1 = x.insertCell(1);
    a1.innerHTML = "<input type='text' style='text-align: right; width:115px;' name='service_product_list_price[" + servln + "]' id='service_product_list_price" + servln + "' size='11' maxlength='50' value='' title='' tabindex='116'   onblur='calculateLine(" + servln + ",\"service_\");'>";

    if (typeof currencyFields !== 'undefined'){
        currencyFields.push("service_product_list_price" + servln);
    }

    var a2 = x.insertCell(2);
    a2.innerHTML = "<input type='text' style='text-align: right; width:90px;' name='service_product_discount[" + servln + "]' id='service_product_discount" + servln + "' size='12' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" + servln + ",\"service_\");' onblur='calculateLine(" + servln + ",\"service_\");'><input type='hidden' name='service_product_discount_amount[" + servln + "]' id='service_product_discount_amount" + servln + "' value=''  />";
    a2.innerHTML += "<select tabindex='116' name='service_discount[" + servln + "]' id='service_discount" + servln + "' onchange='calculateLine(" + servln + ",\"service_\");'>" + discount_hidden + "</select>";

    var b = x.insertCell(3);
    b.innerHTML = "<input type='text' style='text-align: right; width:115px;' name='service_product_unit_price[" + servln + "]' id='service_product_unit_price" + servln + "' size='11' maxlength='50' value='' title='' tabindex='116'   onblur='calculateLine(" + servln + ",\"service_\");'>";

    if (typeof currencyFields !== 'undefined'){
        currencyFields.push("service_product_unit_price" + servln);
    }
    var c = x.insertCell(4);
    c.innerHTML = "<input type='text' style='text-align: right; width:90px;' name='service_vat_amt[" + servln + "]' id='service_vat_amt" + servln + "' size='11' maxlength='250' value='' title='' tabindex='116' readonly='readonly'>";
    c.innerHTML += "<select tabindex='116' name='service_vat[" + servln + "]' id='service_vat" + servln + "' onchange='calculateLine(" + servln + ",\"service_\");'>" + vat_hidden + "</select>";
    if (typeof currencyFields !== 'undefined'){
        currencyFields.push("service_vat_amt" + servln);
    }

    var e = x.insertCell(5);
    e.innerHTML = "<input type='text' style='text-align: right; width:115px;' name='service_product_total_price[" + servln + "]' id='service_product_total_price" + servln + "' size='11' maxlength='50' value='' title='' tabindex='116' readonly='readonly'><input type='hidden' name='service_group_number[" + servln + "]' id='service_group_number" + servln + "' value='"+ groupid +"'>";

    if (typeof currencyFields !== 'undefined'){
        currencyFields.push("service_product_total_price" + servln);
    }
    var f = x.insertCell(6);
    f.innerHTML = "<input type='hidden' name='service_deleted[" + servln + "]' id='service_deleted" + servln + "' value='0'><input type='hidden' name='service_id[" + servln + "]' id='service_id" + servln + "' value=''><button type='button' class='button' id='service_delete_line" + servln + "' value='" + SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_PRODUCT_LINE') + "' tabindex='116' onclick='markLineDeleted(" + servln + ",\"service_\")'><img src='themes/default/images/id-ff-clear.png' alt='" + SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_PRODUCT_LINE') + "'></button><br>";

    servln++;

    return servln - 1;
}

/**
 * Insert product Header
 */
function insertProductHeader(tableid){
    
	tablehead = document.createElement("thead");
	tablehead.id = tableid +"_head";
	tablehead.style.display="none";
	document.getElementById(tableid).appendChild(tablehead);

	var x=tablehead.insertRow(-1);
	x.id='product_head';

	var a=x.insertCell(0);
	a.style.color="rgb(68,68,68)";
	a.innerHTML="Dealership"; //SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_QUANITY');
               
        // modded Bill forsyth, 03/05/2016
	var b=x.insertCell(1);
	b.style.color="rgb(68,68,68)";
	b.innerHTML="Brand" ; // SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_NAME');

        var b1=x.insertCell(2);
        b1.colSpan = "2"; // bill F
        b1.style.color="rgb(68,68,68)";
        b1.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_PART_NUMBER');
    
        // modded Bill forsyth, 02/05/2016
        // Variant
       // var b2=x.insertCell(3);
      //  b2.colSpan = "2";
     //   b2.style.color="rgb(68,68,68)";
      //  b2.innerHTML='Variant'; //SUGAR.language.get(module_sugar_grp1, 'LBL_VARIANT');
                
	var c=x.insertCell(3);
	c.style.color="rgb(68,68,68)";
	c.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_LIST_PRICE');
        
        // modded Bill forsyth, 03/05/2016
        // Predelivery
        var c1=x.insertCell(4);
	c1.style.color="rgb(68,68,68)";
	c1.innerHTML="Pre-delivery"; //SUGAR.language.get(module_sugar_grp1, 'LBL_UNIT_PRICE');
        // Rego+CTP 
	var f=x.insertCell(5);
	f.style.color="rgb(68,68,68)";
	f.innerHTML="Rego + CTP"; // SUGAR.language.get(module_sugar_grp1, 'LBL_LIST_PRICE');

        // Customer discount type
	var d=x.insertCell(6);
	d.style.color="rgb(68,68,68)";
	d.innerHTML="Cust. Discount" ;//SUGAR.language.get(module_sugar_grp1, 'LBL_DISCOUNT_AMT');

         // mats accessories
	var d2=x.insertCell(7);         
        d2.style.color="rgb(68,68,68)";
	d2.innerHTML="Mats" ;//SUGAR.language.get(module_sugar_grp1, 'LBL_DISCOUNT_AMT');
        // tint accessories
	var d3=x.insertCell(8);         
        d3.style.color="rgb(68,68,68)";
	d3.innerHTML="Tint" 
         // paint accessories
	var d4=x.insertCell(9);         
        d4.style.color="rgb(68,68,68)";
	d4.innerHTML="Paint" 
         // towbar accessories
	var d5=x.insertCell(10);         
        d5.style.color="rgb(68,68,68)";
	d5.innerHTML="Towbar" 
        
        // other accessories
	var d5=x.insertCell(11);         
        d5.style.color="rgb(68,68,68)";
	d5.innerHTML="Other Options" 
        
        ///////////////////////////////////////
        var e=x.insertCell(12);
	e.style.color="rgb(68,68,68)";
	e.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_VAT_AMT');
         // luxury car tax
	var g=x.insertCell(13);
	g.style.color="rgb(68,68,68)";
	g.innerHTML="LCT"; // SUGAR.language.get(module_sugar_grp1, 'LBL_LIST_PRICE');
        // stamp duty
        var h=x.insertCell(14);
	h.style.color="rgb(68,68,68)";
	h.innerHTML="Stamp Duty"; // SUGAR.language.get(module_sugar_grp1, 'LBL_LIST_PRICE');

	var i=x.insertCell(15);
	i.style.color="rgb(68,68,68)";
	i.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_TOTAL_PRICE');

	var j=x.insertCell(16);
	j.style.color="rgb(68,68,68)";
	j.innerHTML='&nbsp;';
}

/**
 * Insert service Header
 */
function insertServiceHeader(tableid){
	tablehead = document.createElement("thead");
	tablehead.id = tableid +"_head";
	tablehead.style.display="none";
	document.getElementById(tableid).appendChild(tablehead);

	var x=tablehead.insertRow(-1);
	x.id='service_head';

	var a=x.insertCell(0);
	a.colSpan = "4";
	a.style.color="rgb(68,68,68)";
	a.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_NAME');

        var b=x.insertCell(1);
        b.style.color="rgb(68,68,68)";
        b.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_LIST_PRICE');

        var c=x.insertCell(2);
        c.style.color="rgb(68,68,68)";
        c.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_DISCOUNT');

	var d=x.insertCell(3);
	d.style.color="rgb(68,68,68)";
	d.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_PRICE');

	var e=x.insertCell(4);
	e.style.color="rgb(68,68,68)";
	e.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_VAT_AMT');

	var f=x.insertCell(5);
	f.style.color="rgb(68,68,68)";
	f.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_TOTAL_PRICE');

	var g=x.insertCell(6);
	g.style.color="rgb(68,68,68)";
	g.innerHTML='&nbsp;';
}

/**
 * Insert Group
 */
function insertGroup()
{

        if(!enable_groups && groupn > 0){
            return;
        }
	var tableBody = document.createElement("tr");
	tableBody.id = "group_body"+groupn;
	document.getElementById('lineItems').appendChild(tableBody);

	var a=tableBody.insertCell(0);
	a.colSpan="100";
        var table = document.createElement("table");
	table.id = "group"+groupn;
        
        if(enable_groups){
            table.style.border = '1px grey solid';
            table.style.borderRadius = '4px';
            table.border="1";
        }
	table.style.whiteSpace = 'nowrap';

	table.width = '950';
	a.appendChild(table);

	tableheader = document.createElement("thead");
	table.appendChild(tableheader);
	var header_row=tableheader.insertRow(-1);


        if(enable_groups){
            var header_cell = header_row.insertCell(0);
            header_cell.scope="row";
            header_cell.colSpan="8";
            header_cell.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_GROUP_NAME')+":&nbsp;&nbsp;<input name='group_name[]' id='"+ table.id +"name' size='30' maxlength='255'  title='' tabindex='120' type='text'><input type='hidden' name='group_id[]' id='"+ table.id +"id' value=''><input type='hidden' name='group_group_number[]' id='"+ table.id +"group_number' value='"+groupn+"'>";

	    var header_cell_del = header_row.insertCell(1);
	    header_cell_del.scope="row";
	    header_cell_del.innerHTML="<span title='" + SUGAR.language.get(module_sugar_grp1, 'LBL_DELETE_GROUP') + "' style='float: right;'><a style='cursor: pointer;' id='deleteGroup' tabindex='116' onclick='markGroupDeleted("+groupn+")'><img src='themes/default/images/id-ff-clear.png' alt='X'></a></span><input type='hidden' name='group_deleted[]' id='"+ table.id +"deleted' value='0'>";
        }



	var productTableHeader = document.createElement("thead");
	table.appendChild(productTableHeader);
	var productHeader_row=productTableHeader.insertRow(-1);
	var productHeader_cell = productHeader_row.insertCell(0);
	productHeader_cell.colSpan="100";
	var productTable = document.createElement("table");
	productTable.id = "product_group"+groupn;
	productHeader_cell.appendChild(productTable);

	insertProductHeader(productTable.id);

	var serviceTableHeader = document.createElement("thead");
	table.appendChild(serviceTableHeader);
	var serviceHeader_row=serviceTableHeader.insertRow(-1);
	var serviceHeader_cell = serviceHeader_row.insertCell(0);
	serviceHeader_cell.colSpan="100";
	var serviceTable = document.createElement("table");
	serviceTable.id = "service_group"+groupn;
	serviceHeader_cell.appendChild(serviceTable);

	insertServiceHeader(serviceTable.id);


	/*tablebody = document.createElement("tbody");
	table.appendChild(tablebody);
	var body_row=tablebody.insertRow(-1);
	var body_cell = body_row.insertCell(0);
	body_cell.innerHTML+="&nbsp;";*/

	tablefooter = document.createElement("tfoot");
	table.appendChild(tablefooter);
	var footer_row=tablefooter.insertRow(-1);
	var footer_cell = footer_row.insertCell(0);
	footer_cell.scope="row";
	footer_cell.colSpan="20";
	footer_cell.innerHTML="<input type='button' tabindex='116' class='button' value='"+SUGAR.language.get(module_sugar_grp1, 'LBL_ADD_PRODUCT_LINE')+"' id='"+productTable.id+"addProductLine' onclick='insertProductLine(\""+productTable.id+"\",\""+groupn+"\")' />";
	footer_cell.innerHTML+=" <input type='button' tabindex='116' class='button' value='"+SUGAR.language.get(module_sugar_grp1, 'LBL_ADD_SERVICE_LINE')+"' id='"+serviceTable.id+"addServiceLine' onclick='insertServiceLine(\""+serviceTable.id+"\",\""+groupn+"\")' />";
        //          // Bill f modd 03/06/2016
//        if(enable_groups && false){
//		footer_cell.innerHTML+="<span style='float: right;'>"+SUGAR.language.get(module_sugar_grp1, 'LBL_TOTAL_AMT')+":&nbsp;&nbsp;<input name='group_total_amt[]' id='"+ table.id +"total_amt' size='21' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";
//
//		var footer_row2=tablefooter.insertRow(-1);
//		var footer_cell2 = footer_row2.insertCell(0);
//		footer_cell2.scope="row";
//		footer_cell2.colSpan="20";
//		footer_cell2.innerHTML="<span style='float: right;'>"+SUGAR.language.get(module_sugar_grp1, 'LBL_DISCOUNT_AMOUNT')+":&nbsp;&nbsp;<input name='group_discount_amount[]' id='"+ table.id +"discount_amount' size='21' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";
//
//		var footer_row3=tablefooter.insertRow(-1);
//		var footer_cell3 = footer_row3.insertCell(0);
//		footer_cell3.scope="row";
//		footer_cell3.colSpan="20";
//		footer_cell3.innerHTML="<span style='float: right;'>"+SUGAR.language.get(module_sugar_grp1, 'LBL_SUBTOTAL_AMOUNT')+":&nbsp;&nbsp;<input name='group_subtotal_amount[]' id='"+ table.id +"subtotal_amount' size='21' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";
//
//		var footer_row4=tablefooter.insertRow(-1);
//		var footer_cell4 = footer_row4.insertCell(0);
//		footer_cell4.scope="row";
//		footer_cell4.colSpan="20";
//		footer_cell4.innerHTML="<span style='float: right;'>"+SUGAR.language.get(module_sugar_grp1, 'LBL_TAX_AMOUNT')+":&nbsp;&nbsp;<input name='group_tax_amount[]' id='"+ table.id +"tax_amount' size='21' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";
//
//                if(document.getElementById('subtotal_tax_amount') !== null){
//                    var footer_row5=tablefooter.insertRow(-1);
//                    var footer_cell5 = footer_row5.insertCell(0);
//                    footer_cell5.scope="row";
//                    footer_cell5.colSpan="20";
//                    footer_cell5.innerHTML="<span style='float: right;'>"+SUGAR.language.get(module_sugar_grp1, 'LBL_SUBTOTAL_TAX_AMOUNT')+":&nbsp;&nbsp;<input name='group_subtotal_tax_amount[]' id='"+ table.id +"subtotal_tax_amount' size='21' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";
//
//                    if (typeof currencyFields !== 'undefined'){
//                        currencyFields.push("" + table.id+ 'subtotal_tax_amount');
//                    }
//                }
//
//		var footer_row6=tablefooter.insertRow(-1);
//		var footer_cell6 = footer_row6.insertCell(0);
//		footer_cell6.scope="row";
//		footer_cell6.colSpan="20";
//		footer_cell6.innerHTML="<span style='float: right;'>"+SUGAR.language.get(module_sugar_grp1, 'LBL_GROUP_TOTAL')+":&nbsp;&nbsp;<input name='group_total_amount[]' id='"+ table.id +"total_amount' size='21' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";
//
//            if (typeof currencyFields !== 'undefined'){
//                currencyFields.push("" + table.id+ 'total_amt');
//                currencyFields.push("" + table.id+ 'discount_amount');
//                currencyFields.push("" + table.id+ 'subtotal_amount');
//                currencyFields.push("" + table.id+ 'tax_amount');
//                currencyFields.push("" + table.id+ 'total_amount');
//            }
//        }
        //
        // Bill f modd 19/05/2016
        // Hide the 'Add Service' button in each group
        var gstr ="#service_group" + String(groupn) + "addServiceLine";
        SUGAR.util.doWhen("typeof $ != 'undefined'", function(){
            YAHOO.util.Event.onDOMReady(function(){ $(gstr).hide(); });
        });
        ////
	groupn++;
        
        
        
	return groupn -1;
}

/**
 * Mark Group Deleted
 */
function markGroupDeleted(gn)
{
	document.getElementById('group_body' + gn).style.display = 'none';

	var rows = document.getElementById('group_body' + gn).getElementsByTagName('tbody');

	for (x=0; x < rows.length; x++) {
		var input = rows[x].getElementsByTagName('button');
		for (y=0; y < input.length; y++) {
			if (input[y].id.indexOf('delete_line') != -1) {
				input[y].click();
			}
		}
	}

}

/**
 * Mark line deleted
 */
function markLineDeleted(ln, key)
{
	// collapse line; update deleted value
	document.getElementById(key + 'body' + ln).style.display = 'none';
	document.getElementById(key + 'deleted' + ln).value = '1';
    document.getElementById(key + 'delete_line' + ln).onclick = '';
    var groupid = 'group' + document.getElementById(key + 'group_number' + ln).value;

    if(checkValidate('EditView',key+'product_id' +ln)){
        removeFromValidate('EditView',key+'product_id' +ln);
    }

    calculateTotal(groupid);
    calculateTotal();
}

function fixnum(n){
    return (n == '' || n == null) ? 0: parseFloat(n);
}

/**
 * Calculate Line Values
 *  Bill forsyth 
 *  Viking Vehicles 
 Drive-Away-Price = List Price
                  + Dealer-Pre-delivery
                  - appropriate discount
                  + Mats
                  + Tint
                  + Towbar
                  + Paint
                  + GST ( List Price
                        + Dealer-Pre-delivery
                        + Mats
                        + Tint
                        + Towbar
                        + Paint
                        + other accessories
                        )
                  + Registration
                  + CTP
                  + LCT (Luxury Car Tax where appropriate)
                  +Stamp duty

 */
function calculateLine(ln, key){
    // Modded by Bill forsyth 03/05/2016
    //
    var required = 'product_list_price';
    
    if(document.getElementById(key + required + ln) === null){
        return;
        // required = 'predelivery_c';
    }

    if (document.getElementById(key + 'name' + ln).value === '' ||
        document.getElementById(key + required + ln).value === ''){
        return;
    }
    
    if(key === "product_" && document.getElementById(key + 'product_qty' + ln) !== null 
            && document.getElementById(key + 'product_qty' + ln).value === '') {
        // init quantity if not set 
        document.getElementById(key + 'product_qty' + ln).value =1;
    }  
    ////////////////////////////////////////////////////////////////////////////
    var discount = 0; //get_value(key + 'product_discount' + ln);
    var productQty = 1;
    var listPrice = get_value(key + 'product_list_price' + ln);
    var unitPrice = listPrice;
    var mats = 0;
    if (document.getElementById(key +'select_mats_c' + ln).checked)  {    
        mats = fixnum(get_value(key + 'mats_c' + ln));
    } 
    var tint = 0; 
    if (document.getElementById(key +'select_tint_c' + ln).checked)  {    
        tint = fixnum(get_value(key + 'tint_c' + ln));
    }
    var tb = 0;  
    if (document.getElementById(key +'select_towbar_c' + ln).checked)  {    
        tb = fixnum(get_value(key + 'towbar_c' + ln));
    }
    var paint = 0; 
    if (document.getElementById(key +'select_paint_c' + ln).checked)  {    
        paint = fixnum(get_value(key + 'paint_c' + ln));
    }
    var other = fixnum(get_value(key + 'other_accessories_c' + ln));
    var dis = document.getElementById(key + 'discount' + ln).value;
    ////////////////////////////////////////////////////////////////////////////   
    if(listPrice > 0 && dis !== null &&
       document.getElementById(key + 'product_discount' + ln) !== null)
    {
       //  Discount type
        
        dis = dis.toLowerCase();
        // Discounts are: fleet, novated, none
        if(dis == 'monthly') {
            discount = fixnum(get_value(key + 'best_monthly_discount_c' + ln));
            document.getElementById(key + 'item_description' + ln).value 
                    = disclaimer_part1 + " Best Monthly.\n" + disclaimer_part2;
        } else if(dis == 'fleet') {  
            var d1 = fixnum(get_value(key + 'national_fleet_discount_c' + ln));
            var d2 = fixnum(get_value(key + 'general_fleet_discount_c' + ln)); 
            discount = (d1 > d2)? d1: d2;  
            document.getElementById(key + 'item_description' + ln).value 
                    = disclaimer_part1 + " Fleet.\n" + disclaimer_part2;
        }
        else {
            document.getElementById(key + 'item_description' + ln).value 
                    = disclaimer_part2;
        }
               
        if (discount == '' || discount == null) discount = 0;
        //
        // A little sanity checking
        if( discount > listPrice || dis == 'none') {  
          //  document.getElementById(key + 'unit_price' + ln).value = format2Number(listPrice);
            document.getElementById(key + 'product_discount' + ln).value = '0.00';
            discount = 0;
        } 
        document.getElementById(key + 'product_discount' + ln).value = format2Number(discount); 
    }
    //
    unitPrice = listPrice - discount;
    //
    // GST
    var accessories_cost = mats + tint + paint + tb + other ;
    var vat = unformatNumber(document.getElementById(key + 'vat' + ln).value,',','.');
    var productTotalPrice = unitPrice + accessories_cost;
    var totalvat = (productTotalPrice * vat) / 100.0; 
    var KmPerL = fixnum(get_value(key + 'fuel_economy_combined_c' + ln));
     // Luxury Car Tax (LCT)
    var LCT  =  calculate_LCT(productTotalPrice, totalvat, KmPerL);
    //
    var predelivery = fixnum(get_value(key + 'predelivery_c' + ln))
    // Stamp duty (SD)
    var pistons = fixnum(get_value(key + 'engine_cylinders_c' + ln));
    var elec = document.getElementById(key + 'hybrid_or_electric_c' + ln).value ? true: false;
    var state = document.getElementById(key + 'state_c' + ln).value;
    var SD = calculate_stampduty(state, listPrice, totalvat, predelivery, accessories_cost, KmPerL, elec, pistons);
    //
    // 
    var vikingfee = fixnum(document.getElementById("vv_fees").value);
    var clientfee = fixnum(document.getElementById("vv_customer_fees").value);
    //
    // Viking Fee and Client Fee are added onto dealer pre-delivery charges, so that 
    // that they are not immediately visible.    
    var predelivery_and_fees = predelivery + vikingfee + clientfee;
    //    
    var rego_ctp = fixnum(get_value(key + 'registration_and_ctp_c' + ln));
    //   
    productTotalPrice = productTotalPrice + totalvat +  rego_ctp + SD + LCT + predelivery_and_fees; 
    //
    if(document.getElementById(key + 'product_qty' + ln) !== null){
        productQty = unformat2Number(document.getElementById(key + 'product_qty' + ln).value);
        Quantity_format2Number(ln);
    }
    //
    productTotalPrice *= productQty;
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Update the form
    //document.getElementById(key + 'product_unit_price' + ln).value =format2Number(unitPrice);
    document.getElementById(key + 'product_list_price' + ln).value = format2Number(listPrice);
    document.getElementById(key + 'product_discount' + ln).value = format2Number(unformat2Number(document.getElementById(key + 'product_discount' + ln).value));
    document.getElementById(key + 'product_discount_amount' + ln).value = format2Number(-discount, 6);
    document.getElementById(key + 'luxury_car_tax_c' + ln).value = format2Number(LCT);
    
    
    document.getElementById(key + 'stamp_duty_c' + ln).value = format2Number(SD);
    document.getElementById(key + 'vat_amt' + ln).value = format2Number(totalvat);
    document.getElementById(key + 'predelivery_and_fees' + ln).value = format2Number(predelivery_and_fees);
    document.getElementById(key + 'product_total_price' + ln).value = format2Number(productTotalPrice);
    ////////////////////////////////////////////////////////////////////////////////////
    var groupid = 0;
    if(enable_groups){
        groupid = document.getElementById(key + 'group_number' + ln).value;
    }
    groupid = 'group' + groupid;
    calculateTotal(groupid);
    calculateTotal();

}

function calculateAllLines() {
    var row = document.getElementById('lineItems').getElementsByTagName('tbody');
    var length = row.length;
    //
    for (k=0; k < length; k++) {
        var input = row[k].getElementsByTagName('input');
        var key = input[0].id.split('_')[0]+'_';
        var ln = input[0].id.slice(-1);
        calculateLine(ln, key);
    }
}

/**
 * Calculate totals
 */
function calculateTotal(key)
{
    if (typeof key === 'undefined') {  key = 'lineItems'; }
    var row = document.getElementById(key).getElementsByTagName('tbody');
    if(key == 'lineItems') key = '';
    var length = row.length;
    var head = {};
    var tot_amt = 0;
    var subtotal = 0;
    var dis_tot = 0;
    var tax = 0;
    // modded by bill forsyth
    // 10/05/2016
    for (i=0; i < length; i++) {
        var qty = 1;
        var tmp = 0;
        var list = 0;
        var unit = null;
        var deleted = 0;
        var dis_amt = 0;
        var product_vat_amt = 0;

        var input = row[i].getElementsByTagName('input');
        for (j=0; j < input.length; j++) {
            if (input[j].id.indexOf('product_qty') != -1) {
                qty = unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('product_list_price') != -1)
            {
                list += unformat2Number(input[j].value);
            }
            
            if (input[j].id.indexOf('product_tint_c_amount') != -1)
            {
                tmp = fixnum(unformat2Number(input[j].value));
                if (tmp > 0) {
                    tmp /= qty;
                    list += tmp;
                }
            }
            if (input[j].id.indexOf('product_paint_c_amount') != -1)
            {
                tmp = fixnum(unformat2Number(input[j].value));
                if (tmp > 0) {
                    tmp /= qty;
                    list += tmp;
                }
            }
            if (input[j].id.indexOf('product_towbar_c_amount') != -1)
            {
                tmp = fixnum(unformat2Number(input[j].value));
                 if (tmp > 0) {
                    tmp /= qty;
                    list += tmp;
                }
            }
            if (input[j].id.indexOf('product_mats_c_amount') != -1)
            {
                tmp = fixnum(unformat2Number(input[j].value));
                if (tmp > 0) {
                    tmp /= qty;
                    list += tmp;
                }
            }
            
            if (input[j].id.indexOf('product_other_accessories_c_amount') != -1)
            {
                list += unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('product_registration_and_ctp_c') != -1)
            {
                list += unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('product_stamp_duty_c') != -1)
            {
                list += unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('product_luxury_car_tax_c') != -1)
            {
                list += unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('product_predelivery_c') != -1)
            {
                list += unformat2Number(input[j].value);
            }
            //////////////////////////////////////////////////
            if (input[j].id.indexOf('product_unit_price') != -1)
            {
                unit = unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('product_discount_amount') != -1)
            {
                dis_amt = unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('vat_amt') != -1)
            {
                product_vat_amt = unformat2Number(input[j].value);
            }
            if (input[j].id.indexOf('deleted') != -1) {
                deleted = input[j].value;
            }
        }

        if(deleted != 1 && key !== ''){
            head[row[i].parentNode.id] = 1;
        } else if(key !== '' && head[row[i].parentNode.id] != 1){
            head[row[i].parentNode.id] = 0;
        }

        if (qty !== 0 && list !== 0 && deleted != 1) {
            tot_amt += list * qty;
        } else if (qty !== 0 && unit !== null && deleted != 1) {
            tot_amt += unit * qty;
        }

        if (dis_amt !== 0 && deleted != 1) {
            dis_tot += dis_amt * qty;
        }
        if (product_vat_amt !== 0 && deleted != 1) {
            tax += product_vat_amt;
        }
    }

    for(var h in head){
        if (head[h] != 1 && document.getElementById(h + '_head') !== null) {
            document.getElementById(h + '_head').style.display = "none";
        }
    }

    subtotal = tot_amt + dis_tot;
  

    set_value(key+'total_amt',tot_amt);
   set_value(key+'subtotal_amount',subtotal);
    set_value(key+'discount_amount',dis_tot);

    var shipping = get_value(key+'shipping_amount');

    var shippingtax = get_value(key+'shipping_tax');

    var shippingtax_amt = shipping * (shippingtax/100);

    set_value(key+'shipping_tax_amt',shippingtax_amt);

    tax += shippingtax_amt;

    set_value(key+'tax_amount',tax);

    set_value(key+'subtotal_tax_amount',subtotal + tax);
    set_value(key+'total_amount',subtotal + tax + shipping);
}//

function set_value(id, value){
    if(document.getElementById(id) !== null)
    {
        document.getElementById(id).value = format2Number(value);
    }
}

function get_value(id){
    if(document.getElementById(id) !== null)
    {
        return unformat2Number(document.getElementById(id).value);
    }
    return 0;
}


function unformat2Number(num)
{
    return fixnum(unformatNumber(num, num_grp_sep, dec_sep));
}

function format2Number(str, sig)
{
    if (typeof sig === 'undefined') { sig = sig_digits; }
    num = Number(str);
    if(sig == 2){
        str = formatCurrency(num);
    }
    else{
        str = num.toFixed(sig);
    }

    str = str.split(/,/).join('{,}').split(/\./).join('{.}');
    str = str.split('{,}').join(num_grp_sep).split('{.}').join(dec_sep);

    return str;
}

function formatCurrency(strValue)
{
    strValue = strValue.toString().replace(/\$|\,/g,'');
    dblValue = parseFloat(strValue);

    blnSign = (dblValue == (dblValue = Math.abs(dblValue)));
    dblValue = Math.floor(dblValue*100+0.50000000001);
    intCents = dblValue%100;
    strCents = intCents.toString();
    dblValue = Math.floor(dblValue/100).toString();
    if(intCents<10)
        strCents = "0" + strCents;
    for (var i = 0; i < Math.floor((dblValue.length-(1+i))/3); i++)
        dblValue = dblValue.substring(0,dblValue.length-(4*i+3))+','+
            dblValue.substring(dblValue.length-(4*i+3));
    return (((blnSign)?'':'-') + dblValue + '.' + strCents);
}

function Quantity_format2Number(ln)
{
    var str = '';
    var qty=unformat2Number(document.getElementById('product_product_qty' + ln).value);
    if(qty === null){qty = 1;}

    if(qty === 0){
        str = '0';
    } else {
        str = format2Number(qty);
        if(sig_digits){
            str = str.replace(/0*$/,'');
            str = str.replace(dec_sep,'~');
            str = str.replace(/~$/,'');
            str = str.replace('~',dec_sep);
        }
    }

    document.getElementById('product_product_qty' + ln).value=str;
}

function formatNumber(n, num_grp_sep, dec_sep, round, precision) {
    if (typeof num_grp_sep == "undefined" || typeof dec_sep == "undefined") {
        return n;
    }
    if(n === 0) n = '0';

    n = n ? n.toString() : "";
    if (n.split) {
        n = n.split(".");
    } else {
        return n;
    }
    if (n.length > 2) {
        return n.join(".");
    }
    if (typeof round != "undefined") {
        if (round > 0 && n.length > 1) {
            n[1] = parseFloat("0." + n[1]);
            n[1] = Math.round(n[1] * Math.pow(10, round)) / Math.pow(10, round);
            n[1] = n[1].toString().split(".")[1];
        }
        if (round <= 0) {
            n[0] = Math.round(parseInt(n[0], 10) * Math.pow(10, round)) / Math.pow(10, round);
            n[1] = "";
        }
    }
    if (typeof precision != "undefined" && precision >= 0) {
        if (n.length > 1 && typeof n[1] != "undefined") {
            n[1] = n[1].substring(0, precision);
        } else {
            n[1] = "";
        }
        if (n[1].length < precision) {
            for (var wp = n[1].length; wp < precision; wp++) {
                n[1] += "0";
            }
        }
    }
    regex = /(\d+)(\d{3})/;
    while (num_grp_sep !== "" && regex.test(n[0])) {
        n[0] = n[0].toString().replace(regex, "$1" + num_grp_sep + "$2");
    }
    return n[0] + (n.length > 1 && n[1] !== "" ? dec_sep + n[1] : "");
}

function check_form(formname) {
    calculateAllLines();
    if (typeof(siw) != 'undefined' && siw && typeof(siw.selectingSomething) != 'undefined' && siw.selectingSomething)
        return false;
    return validate_form(formname, '');
}

function calculate_stampduty(state, price, gst, predelivery, accessories, green_rating, hybrid_electric, engine_cylinders)
{
    var sd = 0.0;                                           // Stamp duty accumulator
    // 
    // @Package VV CRM
    // @Author Bill forsyth
    // @Date 16/05/2016
    //
    // Stamp Duty Data taken from (except for ACT/NT, see sources below):
    // http://www.carsguide.com.au/car-advice/how-much-for-stamp-duty-on-your-car-26997
    // 
    // For NEW PRIVATE PASSENGER vehicles only, state by state,
    // stamp duty is calculated as:
    switch(state) {
        case 'NSW': 
            // Source: Roads and Maritime Services [http://www.rms.nsw.gov.au/registration/feesconcessions/stampduty.html]
            // NSW include GST....
            var threshold = 45000.0;
            var base = threshold * 0.03;                //  1350.0 = 3% of 45000
            var unit_price = price + gst;
            //
            if(unit_price < threshold) { 
                sd = base;
            } else {
                sd = base + ((unit_price - threshold) * 0.05);  // 5% on price above base
            }                
            break;
        case 'QLD':
            // Source: QLD Office of State Revenue [http://www.osr.qld.gov.au/duties/about-duties/rates-of-duty.shtml?showta...
            if (hybrid_electric) {
                sd = price * 0.02;                      // 2% for hybrid or electric cars
            } else {
                switch (engine_cylinders) {
                    case 0: 
                        alert("Cannot calculate Stamp Duty on Queensland vehicle with 0 engine cylinders!");
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        sd = price * 0.03;              // 3% 1-4 cyl
                        break;
                    case 5:
                    case 6:
                        sd = price * 0.035;             // 3.5% 5-6 cyl
                        break;
                    default:
                        sd = price * 0.04;              // 4% 7+ cyl                       
                }
            }                
            break;
        case 'SA': 
            // Source: Revenue SA [http://www.revenuesa.sa.gov.au/stamps/sdmotor.html#Rateofstampduty]
            if (price <= 1000){
                sd = price * 0.01;                      // 1%
            } else if (price > 1000 && price <= 2000){
                sd = 10.0 + price * 0.02;               // $10 + 2% over $1000
            } else if (price > 2000 && price <= 3000){
                sd = 30.0 + price * 0.03;               // $30 + 3% over $2000
            } else if (price > 3000 ){
                sd = 60.0 + price * 0.04;               // $60 + 4% over $3000
            }
            break;
        case 'TAS': 
            // Source: Tasmania State Revenue Office [http://www.transport.tas.gov.au/fees/duty_rates]
            if (price <= 600) {
               sd = 600.0;
            } else if(price > 600 && price <= 35000) {
               sd = price * 0.03;                       // 3%
            } else if(price > 35000 && price <= 40000) { 
               sd = 1050.0 + ((price - 35000) * 0.11);
            } else {
               sd = price * 0.04;                       // 4%
            }
            break;
        case 'VIC': 
            // Source: State Revenue Office Victoria [http://www.sro.vic.gov.au/SRO/sronav.nsf/childdocs/-34FAD0EFBAFF8BE0CA25... ]
            if (price <= 59113) {
                sd = price * 0.03;
            } else {
                sd = price * 0.04;
            }
            break;
        case 'WA': 
            // Source: Department of Finance WA [http://www.finance.wa.gov.au/cms/content.aspx?id=3085]
            // In WA, Car value includes dealer delivery charges and GST.
            var unit_price = ( price + gst + predelivery );
            if ( unit_price <= 59113) {
                sd = unit_price * 0.03;                 // 3%
            } else {
                sd = unit_price * 0.04;                 // 4%
            }
            break;
        case 'ACT':
            // Source: http://www.revenue.act.gov.au/duties-and-taxes/duties/motor-vehicles
            var unit_price = price + gst;
            var nhundreds = (unit_price % 100);         //price  modulo 100
            
            //
            // Under the ACT scheme, all new light vehicles will continue to have a 
            // performance rating of A, B, C or D. 
            // The scheme allocates vehicles a performance rating 
            // based on their CO2 emissions, showing grams emitted per kilometre
            if (unit_price <= 45000) {
                switch (toUpperCase(green_rating)){
                    case 'A':
                    case '1':
                        sd = 0.0;                           // Nil
                        break;
                    case 'B':
                    case '2':
                        sd = nhundreds;                     // $1 for each $100
                        break;
                    case 'D':
                    case '4':
                        sd = 4.0 * nhundreds;               // $4 for each $100
                        break;
                    case 'C':
                    case '3':
                    default:        // Non-rated vehicles sames a 'C' rated vehicles
                        sd = 3.0 * nhundreds;               // $3 for each $100
                }
            } else if (unit_price > 45000) {
                switch (toUpperCase(green_rating)){
                    case 'A':
                    case '1':
                        sd = 0.0;                           // Nil
                        break;
                    case 'B':
                    case '2':
                        sd = 450.0 + 2.0 * nhundreds;       // $450 + $2 for each $100
                        break;
                    case 'D':
                    case '4':
                        sd = 1800.0 + 6.0 * nhundreds;      // $1800 + $6 for each $100
                        break;
                    case 'C':
                    case '3':
                     default:        // Non-rated vehicles sames a 'C' rated vehicles
                        sd = 1350.0 + 5.0 * nhundreds;      //$1350 +  $5 for each $100
                } 
            }
            break;
            
        case 'NT': 
            // Source: http://www.treasury.nt.gov.au/TaxesRoyaltiesAndGrants/StampDuty/StampDutyCalculators/Pages/MVR-Duty.aspx
            var unit_price = price + accessories + gst;
            var nhundreds = (unit_price % 100);
            //
            sd = 3.0 * nhundreds;                          // 3% of dutiable price = price+accessories+GST
            break;
            
        default:
           // alert("Invalid State: [" + state + "] passed to calculate_stampduty() func");
    }
    return sd;
}
function calculate_LCT(price, gst, fuel_economy /* LitresPer100KM */) {
    // @Package VV CRM
    // @Author Bill forsyth
    // @Date 16/05/2016
    // 
    // calculate Luxury Car Tax
    // Source: https://www.strattonfinance.com.au/car-finance/learn/articles/luxury-car-tax-lct-explained.aspx
    // Source: http://law.ato.gov.au/atolaw/view.htm?docid=%22LCD%2FLCTD20151%2FNAT%2FATO%2F00001%22
    // NOTES:  Luxury Car Tax is applicable to most new and demonstrator cars 
    // where the price (including GST and excluding government 
    // fees and charges such as stamp duty, registration & CTP) 
    // exceeds the Luxury Car Tax threshold. 
    // LCT is charged on the portion of the vehicle's price 
    // that exceeds this threshold. 
    // The Luxury Car Tax threshold is subject to change each financial year.
    //
    // LCT will not apply to certain fuel efficient cars 
    // (cars with combined-cycle fuel consumption of less than 7L/100km) 
    // sold on or after the 3rd of October 2008 
    // whose price is less than a new, 
    // higher fuel-efficient car limit (including GST and excluding government fees 
    // and charges such as stamp duty, registration & CTP).
    //
    //
    var fuel_efficient_threshold = document.getElementById('green_threshold').value;
    var luxury_threshold = document.getElementById('luxury_threshold').value;
    var LCT_rate =  document.getElementById('lct_rate').value; 
    var green_fuel_economy =  document.getElementById('litres_per_100km').value; 
   
    if (LCT_rate === '' || green_fuel_economy === '' || fuel_efficient_threshold === '' ||  luxury_threshold === ''){
        alert("LCT parameters are incomplete. Cannot calculate the LCT.");
        return 0.0;
    }
    
   
    
    //
    var unit_price = price + gst;
    //
    //
    if (unit_price <= luxury_threshold) {
        return 0.00;
    }    
    //
    var threshold = 0;
    //
    if (fuel_economy <= green_fuel_economy) {
        if (unit_price <= fuel_efficient_threshold) {
            return 0.00;
        }
        threshold = fuel_efficient_threshold; 
    } else {
        threshold = luxury_threshold; 
    }
    //
    var LCT = unit_price - threshold;
    //
    // Exclude GST from the amount in excess of the threshold;
    LCT /= 1.1;
    // 
    // Calculate LCT component of that which exceeds the threshold
    LCT *= (LCT_rate / 100.0);
    //
    return LCT;
}