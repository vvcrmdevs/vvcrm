<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-05-14 04:49:38
$dictionary['AOS_Products_Quotes']['fields']['stamp_duty_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['stamp_duty_c']['labelValue']='Stamp Duty';

 

 // created: 2016-04-27 03:51:10
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['reportable']=true;
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['len']=255;

 

 // created: 2016-04-28 00:55:06
$dictionary['AOS_Products_Quotes']['fields']['vat']['default']='';
$dictionary['AOS_Products_Quotes']['fields']['vat']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['vat']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['vat']['help']='Get Stuffed Tax';
$dictionary['AOS_Products_Quotes']['fields']['vat']['comments']='get stuffed tax';
$dictionary['AOS_Products_Quotes']['fields']['vat']['importable']='false';

 

 // created: 2016-04-28 00:58:54
$dictionary['AOS_Products_Quotes']['fields']['product_total_price_usdollar']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_total_price_usdollar']['duplicate_merge']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_total_price_usdollar']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products_Quotes']['fields']['product_total_price_usdollar']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_total_price_usdollar']['enable_range_search']=false;

 

 // created: 2016-04-27 04:05:14
$dictionary['AOS_Products_Quotes']['fields']['paint_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['paint_c']['labelValue']='Paint';

 

 // created: 2016-04-27 03:51:10
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['options']='parent_type_display';
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['reportable']=true;

 

 // created: 2016-05-14 04:50:20
$dictionary['AOS_Products_Quotes']['fields']['best_monthly_discount_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['best_monthly_discount_c']['labelValue']='best monthly discount';

 

 // created: 2016-04-27 03:45:15
$dictionary['AOS_Products_Quotes']['fields']['vat_amt']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['vat_amt']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['vat_amt']['enable_range_search']=false;

 

 // created: 2016-04-27 03:55:33
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price_usdollar']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price_usdollar']['duplicate_merge']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price_usdollar']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price_usdollar']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price_usdollar']['enable_range_search']=false;

 

 // created: 2016-05-16 00:39:00
$dictionary['AOS_Products_Quotes']['fields']['paint_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['paint_c']['labelValue']='Paint';

 

 // created: 2016-04-27 04:03:10
$dictionary['AOS_Products_Quotes']['fields']['variant_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['variant_c']['labelValue']='Variant';

 

 // created: 2016-04-27 03:51:10
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['reportable']=true;
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['len']=255;

 

// created: 2016-04-27 04:09:22
$dictionary["AOS_Products_Quotes"]["fields"]["aos_products_quotes_vik_dealership_1"] = array (
  'name' => 'aos_products_quotes_vik_dealership_1',
  'type' => 'link',
  'relationship' => 'aos_products_quotes_vik_dealership_1',
  'source' => 'non-db',
  'module' => 'vik_Dealership',
  'bean_name' => 'vik_Dealership',
  'vname' => 'LBL_AOS_PRODUCTS_QUOTES_VIK_DEALERSHIP_1_FROM_VIK_DEALERSHIP_TITLE',
);


 // created: 2016-05-14 09:41:45
$dictionary['AOS_Products_Quotes']['fields']['vik_dealership_id_c']['inline_edit']=1;

 

 // created: 2016-05-14 04:50:50
$dictionary['AOS_Products_Quotes']['fields']['general_fleet_discount_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['general_fleet_discount_c']['labelValue']='general fleet discount';

 

 // created: 2016-05-14 04:51:18
$dictionary['AOS_Products_Quotes']['fields']['towbar_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['towbar_c']['labelValue']='Towbar';

 

 // created: 2016-05-14 04:51:39
$dictionary['AOS_Products_Quotes']['fields']['mats_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['mats_c']['labelValue']='Mats';

 

 // created: 2016-05-14 10:12:46
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_unit_price']['enable_range_search']=false;

 

 // created: 2016-05-14 04:52:05
$dictionary['AOS_Products_Quotes']['fields']['select_mats_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['select_mats_c']['labelValue']='Select Mats';

 

 // created: 2016-04-27 03:59:17
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price_usdollar']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price_usdollar']['duplicate_merge']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price_usdollar']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price_usdollar']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price_usdollar']['enable_range_search']=false;

 

 // created: 2016-05-14 04:52:41
$dictionary['AOS_Products_Quotes']['fields']['registration_and_ctp_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['registration_and_ctp_c']['labelValue']='Rego + CTP';

 

 // created: 2016-05-14 04:52:59
$dictionary['AOS_Products_Quotes']['fields']['predelivery_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['predelivery_c']['labelValue']='Pre-Delivery';

 

 // created: 2016-05-16 00:43:53
$dictionary['AOS_Products_Quotes']['fields']['fuel_economy_combined_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['fuel_economy_combined_c']['labelValue']='Fuel Economy Combined';

 

 // created: 2016-05-14 04:53:42
$dictionary['AOS_Products_Quotes']['fields']['national_fleet_discount_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['national_fleet_discount_c']['labelValue']='National fleet Discount';

 

 // created: 2016-04-27 04:07:36
$dictionary['AOS_Products_Quotes']['fields']['product_qty']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_qty']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_qty']['precision']='0';

 

 // created: 2016-05-14 09:42:32
$dictionary['AOS_Products_Quotes']['fields']['dealer_c']['inline_edit']='';
$dictionary['AOS_Products_Quotes']['fields']['dealer_c']['labelValue']='Vendor';

 

 // created: 2016-05-16 00:46:22
$dictionary['AOS_Products_Quotes']['fields']['engine_cylinders_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['engine_cylinders_c']['labelValue']='Engine Cylinders';

 

 // created: 2016-05-14 10:13:54
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['enable_range_search']='1';
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['required']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_list_price']['options']='numeric_range_search_dom';

 

 // created: 2016-05-14 04:54:02
$dictionary['AOS_Products_Quotes']['fields']['select_towbar_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['select_towbar_c']['labelValue']='Select Towbar';

 

 // created: 2016-05-04 01:14:02
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_cost_price']['enable_range_search']=false;

 

 // created: 2016-05-20 03:27:16
$dictionary['AOS_Products_Quotes']['fields']['customer_fee_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['customer_fee_c']['labelValue']='Customer Fee';

 

 // created: 2016-05-19 00:20:28
$dictionary['AOS_Products_Quotes']['fields']['state_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['state_c']['labelValue']='State';

 

 // created: 2016-05-16 00:41:41
$dictionary['AOS_Products_Quotes']['fields']['hybrid_or_electric_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['hybrid_or_electric_c']['labelValue']='Hybrid or Electric';

 

 // created: 2016-05-14 04:54:27
$dictionary['AOS_Products_Quotes']['fields']['select_tint_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['select_tint_c']['labelValue']='Select Tint';

 

 // created: 2016-05-14 04:54:47
$dictionary['AOS_Products_Quotes']['fields']['other_accessories_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['other_accessories_c']['labelValue']='Other Accessories';

 

 // created: 2016-05-07 08:56:49
$dictionary['AOS_Products_Quotes']['fields']['item_description']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['item_description']['merge_filter']='disabled';

 

 // created: 2016-06-09 05:23:18
$dictionary['AOS_Products_Quotes']['fields']['vv_fees_c']['inline_edit']='';
$dictionary['AOS_Products_Quotes']['fields']['vv_fees_c']['labelValue']='VV Fees';

 

 // created: 2016-05-14 04:55:05
$dictionary['AOS_Products_Quotes']['fields']['luxury_car_tax_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['luxury_car_tax_c']['labelValue']='Luxury Car Tax';

 

 // created: 2016-04-27 03:45:52
$dictionary['AOS_Products_Quotes']['fields']['product_total_price']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['product_total_price']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['product_total_price']['enable_range_search']=false;

 

 // created: 2016-05-12 23:18:03
$dictionary['AOS_Products_Quotes']['fields']['discount']['default']='monthly';
$dictionary['AOS_Products_Quotes']['fields']['discount']['len']=100;
$dictionary['AOS_Products_Quotes']['fields']['discount']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['discount']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['discount']['help']='available vehicle discounts';
$dictionary['AOS_Products_Quotes']['fields']['discount']['importable']='false';

 

 // created: 2016-05-14 04:55:27
$dictionary['AOS_Products_Quotes']['fields']['tint_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['tint_c']['labelValue']='Tint';

 

 // created: 2016-05-14 04:55:44
$dictionary['AOS_Products_Quotes']['fields']['select_paint_c']['inline_edit']='1';
$dictionary['AOS_Products_Quotes']['fields']['select_paint_c']['labelValue']='Select Paint';

 

 // created: 2016-04-27 04:10:53
$dictionary['AOS_Products_Quotes']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['name']['unified_search']=false;
$dictionary['AOS_Products_Quotes']['fields']['name']['rows']='4';
$dictionary['AOS_Products_Quotes']['fields']['name']['cols']='20';

 

 // created: 2016-04-28 00:57:31
$dictionary['AOS_Products_Quotes']['fields']['vat_amt_usdollar']['inline_edit']=true;
$dictionary['AOS_Products_Quotes']['fields']['vat_amt_usdollar']['duplicate_merge']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['vat_amt_usdollar']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products_Quotes']['fields']['vat_amt_usdollar']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['vat_amt_usdollar']['enable_range_search']=false;

 

 // created: 2016-06-09 05:23:55
$dictionary['AOS_Products_Quotes']['fields']['vv_customer_fees_c']['inline_edit']='';
$dictionary['AOS_Products_Quotes']['fields']['vv_customer_fees_c']['labelValue']='VV Customer Fees';

 

 // created: 2016-04-27 04:00:28
$dictionary['AOS_Products_Quotes']['fields']['part_number']['inline_edit']=true;

 
?>