<?php
// created: 2016-06-10 00:16:29
$mod_strings = array (
  'LBL_TAX_AMOUNT' => 'GST',
  'LBL_ACCOUNT_INFORMATION' => 'Quote',
  'LBL_ADDRESS_INFORMATION' => 'Client',
  'LBL_LINE_ITEMS' => 'Vehicles',
  'LBL_VIK_DEALERSHIP_AOS_QUOTES_1_FROM_VIK_DEALERSHIP_TITLE' => 'Dealers',
  'LBL_VV_FEES' => 'VV Fees',
  'LBL_VV_CUSTOMER_FEES' => 'VV Customer Fees',
);