<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-04-20 02:28:14
$layout_defs["AOS_Quotes"]["subpanel_setup"]['vik_dealership_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'vik_Dealership',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_VIK_DEALERSHIP_AOS_QUOTES_1_FROM_VIK_DEALERSHIP_TITLE',
  'get_subpanel_data' => 'vik_dealership_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Quotes']['subpanel_setup']['vik_dealership_aos_quotes_1']['override_subpanel_name'] = 'AOS_Quotes_subpanel_vik_dealership_aos_quotes_1';

?>