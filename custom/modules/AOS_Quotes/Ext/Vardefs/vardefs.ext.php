<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-04-27 04:39:42
$dictionary['AOS_Quotes']['fields']['tax_amount']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['tax_amount']['merge_filter']='disabled';
$dictionary['AOS_Quotes']['fields']['tax_amount']['enable_range_search']=false;

 

// created: 2016-04-20 02:28:14
$dictionary["AOS_Quotes"]["fields"]["vik_dealership_aos_quotes_1"] = array (
  'name' => 'vik_dealership_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'vik_Dealership',
  'bean_name' => 'vik_Dealership',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_QUOTES_1_FROM_VIK_DEALERSHIP_TITLE',
);


 // created: 2016-06-10 00:14:27
$dictionary['AOS_Quotes']['fields']['vv_fees_c']['inline_edit']='';
$dictionary['AOS_Quotes']['fields']['vv_fees_c']['labelValue']='VV Fees';

 

 // created: 2016-06-10 00:16:29
$dictionary['AOS_Quotes']['fields']['vv_customer_fees_c']['inline_edit']='';
$dictionary['AOS_Quotes']['fields']['vv_customer_fees_c']['labelValue']='VV Customer Fees';

 
?>