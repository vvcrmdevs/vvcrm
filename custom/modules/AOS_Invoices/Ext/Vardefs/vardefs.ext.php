<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2016-04-20 02:28:50
$dictionary["AOS_Invoices"]["fields"]["vik_dealership_aos_invoices_1"] = array (
  'name' => 'vik_dealership_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'vik_Dealership',
  'bean_name' => 'vik_Dealership',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_INVOICES_1_FROM_VIK_DEALERSHIP_TITLE',
);


 // created: 2016-06-10 00:32:22
$dictionary['AOS_Invoices']['fields']['vv_fees_c']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['vv_fees_c']['labelValue']='VV Fees';

 

 // created: 2016-06-10 00:32:54
$dictionary['AOS_Invoices']['fields']['vv_customer_fees_c']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['vv_customer_fees_c']['labelValue']='VV Customer Fees';

 
?>