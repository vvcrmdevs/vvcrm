<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-04-26 05:47:59
$dictionary['vik_Dealership']['fields']['postcode']['len']='8';

 

 // created: 2016-04-20 04:59:36
$dictionary['vik_Dealership']['fields']['date_entered']['comments']='Date record created';
$dictionary['vik_Dealership']['fields']['date_entered']['merge_filter']='disabled';

 

// created: 2016-04-27 04:09:22
$dictionary["vik_Dealership"]["fields"]["aos_products_quotes_vik_dealership_1"] = array (
  'name' => 'aos_products_quotes_vik_dealership_1',
  'type' => 'link',
  'relationship' => 'aos_products_quotes_vik_dealership_1',
  'source' => 'non-db',
  'module' => 'AOS_Products_Quotes',
  'bean_name' => 'AOS_Products_Quotes',
  'vname' => 'LBL_AOS_PRODUCTS_QUOTES_VIK_DEALERSHIP_1_FROM_AOS_PRODUCTS_QUOTES_TITLE',
);


// created: 2016-04-20 02:26:48
$dictionary["vik_Dealership"]["fields"]["vik_dealership_aos_products_1"] = array (
  'name' => 'vik_dealership_aos_products_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


// created: 2016-04-20 02:28:14
$dictionary["vik_Dealership"]["fields"]["vik_dealership_aos_quotes_1"] = array (
  'name' => 'vik_dealership_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2016-04-20 02:28:50
$dictionary["vik_Dealership"]["fields"]["vik_dealership_aos_invoices_1"] = array (
  'name' => 'vik_dealership_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


 // created: 2016-05-26 01:06:22
$dictionary['vik_Dealership']['fields']['free_delivery_distance_c']['inline_edit']='1';
$dictionary['vik_Dealership']['fields']['free_delivery_distance_c']['labelValue']='Free Delivery Radius';

 

// created: 2016-04-20 02:32:39
$dictionary["vik_Dealership"]["fields"]["vik_dealership_aos_contracts_1"] = array (
  'name' => 'vik_dealership_aos_contracts_1',
  'type' => 'link',
  'relationship' => 'vik_dealership_aos_contracts_1',
  'source' => 'non-db',
  'module' => 'AOS_Contracts',
  'bean_name' => 'AOS_Contracts',
  'side' => 'right',
  'vname' => 'LBL_VIK_DEALERSHIP_AOS_CONTRACTS_1_FROM_AOS_CONTRACTS_TITLE',
);

?>