<?php
// created: 2016-06-01 02:08:24
$mod_strings = array (
  'LBL_DATE_ENTERED' => 'Date',
  'LBL_POSTCODE' => 'Postcode',
  'LBL_FREE_DELIVERY_DISTANCE' => 'Free Delivery Radius',
  'LBL_EDITVIEW_PANEL2' => 'Dealership Contact 1',
  'LBL_EDITVIEW_PANEL3' => 'Dealership Contact 2',
  'LBL_EDITVIEW_PANEL1' => 'Dealership Charges',
  'LNK_NEW_RECORD' => 'Create Dealers',
  'LNK_LIST' => 'View Dealers',
  'LNK_IMPORT_VIK_DEALERSHIP' => 'Import Dealers',
  'LBL_LIST_FORM_TITLE' => 'Dealer List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Dealer',
  'LBL_HOMEPAGE_TITLE' => 'My Dealers',
);