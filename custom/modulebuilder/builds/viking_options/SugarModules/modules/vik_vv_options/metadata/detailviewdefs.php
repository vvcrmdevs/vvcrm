<?php
$module_name = 'vik_vv_options';
$_object_name = 'vik_vv_options';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'form' => 
      array (
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'lct_rate',
            'label' => 'LBL_LCT_RATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'litres_per_100km',
            'label' => 'LBL_LITRES_PER_100KM',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'luxury_threshold',
            'label' => 'LBL_LUXURY_THRESHOLD',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'fuel_efficient_threshold',
            'label' => 'LBL_FUEL_EFFICIENT_THRESHOLD',
          ),
        ),
      ),
    ),
  ),
);
?>
