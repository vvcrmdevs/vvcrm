<?php
$popupMeta = array (
    'moduleMain' => 'vik_Dealership',
    'varName' => 'vik_Dealership',
    'orderBy' => 'vik_dealership.name',
    'whereClauses' => array (
  'name' => 'vik_dealership.name',
  'city' => 'vik_dealership.city',
  'postcode' => 'vik_dealership.postcode',
  'state' => 'vik_dealership.state',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'city',
  5 => 'postcode',
  6 => 'state',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'name' => 'name',
  ),
  'city' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CITY',
    'width' => '10%',
    'name' => 'city',
  ),
  'postcode' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_POSTCODE',
    'width' => '10%',
    'name' => 'postcode',
  ),
  'state' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STATE',
    'width' => '10%',
    'name' => 'state',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '20%',
    'default' => true,
  ),
  'URL' => 
  array (
    'type' => 'url',
    'label' => 'LBL_URL',
    'width' => '10%',
    'default' => true,
  ),
  'STREET' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STREET',
    'width' => '10%',
    'default' => true,
  ),
  'CITY' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CITY',
    'width' => '10%',
    'default' => true,
  ),
  'STATE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STATE',
    'width' => '10%',
    'default' => true,
  ),
  'POSTCODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_POSTCODE',
    'width' => '10%',
    'default' => true,
  ),
  'PREDELIVERY' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PREDELIVERY',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
),
);
