<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Notes',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Dealer',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Dealer List',
  'LBL_MODULE_NAME' => 'Dealer',
  'LBL_MODULE_TITLE' => 'Dealer',
  'LBL_HOMEPAGE_TITLE' => 'My Dealer',
  'LNK_NEW_RECORD' => 'Create Dealer',
  'LNK_LIST' => 'View Dealer',
  'LNK_IMPORT_VIK_DEALERSHIP' => 'Import Dealer',
  'LBL_SEARCH_FORM_TITLE' => 'Search Dealer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_VIK_DEALERSHIP_SUBPANEL_TITLE' => 'Dealer',
  'LBL_NEW_FORM_TITLE' => 'New Dealer',
  'LBL_LANDLINE1' => 'Landline1',
  'LBL_MOBILE1' => 'Mobile1',
  'LBL_EMAIL1' => 'Email1',
  'LBL_CONTACT1' => 'Contact1',
  'LBL_LANDLINE2' => 'Landline2',
  'LBL_MOBILE2' => 'Mobile2',
  'LBL_EMAIL2' => 'Email2',
  'LBL_CONTACT2' => 'Contact2',
  'LBL_URL' => 'URL',
  'LBL_STREET' => 'Street',
  'LBL_CITY' => 'City',
  'LBL_STATE' => 'State',
  'LBL_PREDELIVERY' => 'Pre-Delivery',
  'LBL_POSTCODE' => 'Postcode',
  'LBL_DETAILVIEW_PANEL1' => 'Contact 1',
  'LBL_DETAILVIEW_PANEL2' => 'Contact 2',
  'LBL_EDITVIEW_PANEL1' => 'Charges',
  'LBL_EDITVIEW_PANEL2' => 'Contact 1',
  'LBL_EDITVIEW_PANEL3' => 'Contact 2',
);