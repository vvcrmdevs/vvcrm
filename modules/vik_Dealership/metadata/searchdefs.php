<?php
$module_name = 'vik_Dealership';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'state' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE',
        'width' => '10%',
        'default' => true,
        'name' => 'state',
      ),
      'postcode' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_POSTCODE',
        'width' => '10%',
        'default' => true,
        'name' => 'postcode',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'url' => 
      array (
        'type' => 'url',
        'label' => 'LBL_URL',
        'width' => '10%',
        'default' => true,
        'name' => 'url',
      ),
      'street' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STREET',
        'width' => '10%',
        'default' => true,
        'name' => 'street',
      ),
      'city' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_CITY',
        'width' => '10%',
        'default' => true,
        'name' => 'city',
      ),
      'state' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_STATE',
        'width' => '10%',
        'default' => true,
        'name' => 'state',
      ),
      'postcode' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_POSTCODE',
        'width' => '10%',
        'default' => true,
        'name' => 'postcode',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
