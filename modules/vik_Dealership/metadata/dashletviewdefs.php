<?php
$dashletData['vik_DealershipDashlet']['searchFields'] = array (
  'name' => 
  array (
    'default' => '',
  ),
  'city' => 
  array (
    'default' => '',
  ),
  'postcode' => 
  array (
    'default' => '',
  ),
  'state' => 
  array (
    'default' => '',
  ),
);
$dashletData['vik_DealershipDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'city' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CITY',
    'width' => '10%',
    'default' => true,
  ),
  'state' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STATE',
    'width' => '5%',
    'default' => true,
  ),
  'postcode' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_POSTCODE',
    'width' => '5%',
    'default' => true,
  ),
  'url' => 
  array (
    'type' => 'url',
    'label' => 'LBL_URL',
    'width' => '40%',
    'default' => true,
  ),
  'predelivery' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PREDELIVERY',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
);
