<?php
if(!empty($_REQUEST['dropdown_name']) && !empty($_REQUEST['dropdown_key']) && !empty($_REQUEST['dropdown_field_name']))
{
    global $app_list_strings;
        require_once('modules/ModuleBuilder/MB/ModuleBuilder.php');    
        require_once('modules/ModuleBuilder/parsers/parser.dropdown.php');
        $parser = new ParserDropDown();
        $params = array();
        $_REQUEST['view_package'] = 'studio'; //need this in parser.dropdown.php
        $params['view_package'] = 'studio';
        $params['dropdown_name'] = $_REQUEST['dropdown_name']; //replace with the dropdown name
        $params['dropdown_lang'] = 'en_us';
        $dropdwon_list = $app_list_strings[$params['dropdown_name']];
        foreach($dropdwon_list as $key => $value){
            $drop_list[] = array($key,$value);
        }
        $drop_list[] = array($_REQUEST['dropdown_key'],$_REQUEST['dropdown_value']);
        $json = getJSONobj();
        $params['list_value'] = $json->encode( $drop_list );
        $parser->saveDropDown($params);
        echo json_encode(
                        array(
                        'success'=>'true',
                        'droddown_key' => $_REQUEST['dropdown_key'],
                        'droddown_value' => $_REQUEST['dropdown_value'],
                        'field_name' => $_REQUEST['dropdown_field_name']
                        ));
}
