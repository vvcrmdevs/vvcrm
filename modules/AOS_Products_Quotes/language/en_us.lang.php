<?php
/**
 * Products, Quotations & Invoices modules.
 * Extensions to SugarCRM
 * @package Advanced OpenSales for SugarCRM
 * @subpackage Products
 * @copyright SalesAgility Ltd http://www.salesagility.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author Salesagility Ltd <support@salesagility.com>
 * 
 * 
 * 
 * 
 * 
 *  Yaddy yah
 *  Viking Vehicles:
 *  Modified by Bill Forsyth 29/04/2016
 */

$mod_strings = array (
    'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
    'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => 'Date Created',
    'LBL_DATE_MODIFIED' => 'Date Modified',
    'LBL_MODIFIED' => 'Modified By',
    'LBL_MODIFIED_ID' => 'Modified By Id',
    'LBL_MODIFIED_NAME' => 'Modified By Name',
    'LBL_CREATED' => 'Created By',
    'LBL_CREATED_ID' => 'Created By Id',
    'LBL_DESCRIPTION' => 'Note',
    'LBL_DELETED' => 'Deleted',
    'LBL_NAME' => 'Brand',
    'LBL_CREATED_USER' => 'Created by User',
    'LBL_MODIFIED_USER' => 'Modified by User',
    'LBL_LIST_FORM_TITLE' => 'Products Quotes List',
    'LBL_MODULE_NAME' => 'Line Items',
    'LBL_MODULE_TITLE' => 'Products Quotes: Home',
    'LBL_HOMEPAGE_TITLE' => 'My Products Quotes',
    'LNK_NEW_RECORD' => 'Create Product Quote',
    'LNK_LIST' => 'Products_Quotes',
    'LBL_SEARCH_FORM_TITLE' => 'Search Products_Quotes',
    'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
    'LBL_AOS_PRODUCTS_SUBPANEL_TITLE' => 'Products_Quotes',
    'LBL_NEW_FORM_TITLE' => 'New Product_Quote',
    'LBL_PRODUCT_NAME' => 'Brand',
    'LBL_PRODUCT_QTY' => 'Units',
    'LBL_PRODUCT_COST_PRICE' => 'Registration + CTP',
    'LBL_PRODUCT_LIST_PRICE' => 'List Price (ex GST)',
    'LBL_PRODUCT_UNIT_PRICE' => 'Pre-Delivery Fee',
    'LBL_PRODUCT_DISCOUNT' => 'Discount',
    'LBL_PRODUCT_DISCOUNT_AMOUNT' => 'Discount Amount',
    'LBL_PART_NUMBER' => 'Model',
    'LBL_VARIANT' => 'Variant',
    'LBL_PRODUCT_DESCRIPTION' => 'Description',
    'LBL_DISCOUNT' => 'Discount Type',
    'LBL_VAT_AMT' => 'GST',
    'LBL_VAT' => 'GST Rate',
    'LBL_PRODUCT_TOTAL_PRICE' => 'Drive Away Price',
    'LBL_PRODUCT_NOTE' => 'Note',
    'Quote' => '',
    'LBL_AOS_PRODUCTS_QUOTES_SUBPANEL_TITLE' => 'Vehicle Quotes',
    'LBL_FLEX_RELATE' => 'Related to',
    'LBL_PRODUCT' => 'Vehicle',
    'LBL_SERVICE_MODULE_NAME' => 'Services',
    'LBL_LIST_NUM' => 'Number',
    'LBL_PARENT_ID' => 'Parent ID',
    'LBL_GROUP_NAME' => 'Group',
    'LBL_PRODUCT_COST_PRICE_USDOLLAR' => 'Cost Price (Default Currency)',
    'LBL_PRODUCT_LIST_PRICE_USDOLLAR' => 'List Price (Default Currency)',
    'LBL_PRODUCT_UNIT_PRICE_USDOLLAR' => 'Unit Price (Default Currency)',
    'LBL_PRODUCT_TOTAL_PRICE_USDOLLAR' => 'Drive-Away Price (Default Currency)',
    'LBL_PRODUCT_DISCOUNT_USDOLLAR' => 'Discount (Default Currency)',
    'LBL_PRODUCT_DISCOUNT_AMOUNT_USDOLLAR' => 'Discount Amount (Default Currency)',
    'LBL_VAT_AMT_USDOLLAR' => 'GST Amount (Default Currency)',
    'LBL_PRODUCTS_SERVICES' => 'Product / Service',
    
);
?>
